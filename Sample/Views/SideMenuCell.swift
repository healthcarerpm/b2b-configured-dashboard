//
//  SideMenuCell.swift
//  Sample
//
//  Created by Thejus Jose on 06/04/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var sideMenuContents: UILabel!
    @IBOutlet weak var sideMenuContentImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }//setPassCodeSegue

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
