//
//  TitleView.swift
//  Sample
//
//  Created by Thejus Jose on 20/03/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import UIKit

class TitleView : UIView {
    
    @IBOutlet weak var title : UILabel!
    
    class func instanceFromNib()->TitleView{
        return UINib(nibName: "TitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView as! TitleView
    }
}
