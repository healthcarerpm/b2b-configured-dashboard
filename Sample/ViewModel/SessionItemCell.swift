//
//  SessionItemCell.swift
//  Sample
//
//  Created by Thejus Jose on 13/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit


class SessionItemCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    override func awakeFromNib() {
        startButton.setTitle("Start Session", for: .normal)
        startButton.isEnabled = false
        startButton.alpha = 0.25
        
    }
}
