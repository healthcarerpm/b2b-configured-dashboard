//
//  CheckBox.swift
//  Sample
//
//  Created by Veeresh on 05/10/19.
//  Copyright © 2019 Ackee.Inc. All rights reserved.
//

import Foundation

import UIKit
class CheckBox: UIButton {
 
    let checkedImage = UIImage(named: "checkedbox")! as UIImage
    let uncheckedImage = UIImage(named: "uncheckedbox")! as UIImage
   
    var titleofcheckbox = ""
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            }
            else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.black.cgColor
        self.setImage(uncheckedImage, for: UIControl.State.normal)
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
