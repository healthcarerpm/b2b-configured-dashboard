//
//  CoreDataUtils.swift
//  Sample
//
//  Created by Isham on 06/02/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

class CoreDataUtils  {
    
    fileprivate let isolationQueue: OperationQueue = {
        
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 5
        operationQueue.name = "persistenceOperationQueue"
        return operationQueue
    }()
    
    
    func saveUserdetails(value:[String: Any])  {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "User",
                                                in: managedContext)!
        let user = NSManagedObject(entity: entity, insertInto: managedContext)
        
        let patientId = value ["Id"]!
        let patientFirstName = value ["FirstName"]! //For Test since fname and lname as nil... Remove later
        let patientLastName = value ["LastName"]!
        let roleId = value["RoleId"]
        let customerId = value["CustomerId"]
        let lastAction = value["LastAction"]
        
        user.setValue(patientId, forKeyPath: "id")
        user.setValue(nullToNil(value: patientFirstName as AnyObject), forKeyPath: "firstName")
        user.setValue(nullToNil(value: patientLastName as AnyObject), forKeyPath: "lastName")
        user.setValue(nullToNil(value: roleId as AnyObject), forKeyPath:"roleId")
        user.setValue(nullToNil(value: customerId as AnyObject), forKeyPath: "customerId")
        user.setValue(nullToNil(value: lastAction as AnyObject), forKeyPath: "lastLogin")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveCustomerType(dictionary:[String:Any]) {
        let measurmentidentifier = MeasurementIdentifier.sharedInteractor
        measurmentidentifier.bloodsugar = dictionary["BloodGlucoseId"] as? String
        measurmentidentifier.temperatureID = dictionary["TemperatureId"] as? String
        if dictionary["IsEmployer"] != nil{
            measurmentidentifier.isEmployer = dictionary["IsEmployer"] as! Int
        }
        measurmentidentifier.diastolicID = dictionary["BloodPressureId"] as? String
        measurmentidentifier.weightID = dictionary["WeightId"] as? String
        measurmentidentifier.spo2ID = dictionary["SPO2Id"] as? String
        measurmentidentifier.systolicID = dictionary["BloodPressureId"] as? String
        measurmentidentifier.pulseID = dictionary["HeartrateId"] as? String
        
        measurmentidentifier.customerId = dictionary["Id"] as? String
        measurmentidentifier.weightstart = dictionary["WeightStart"] as? Double
        measurmentidentifier.weightEnd = dictionary["WeightEnd"] as? Double
        measurmentidentifier.Spo2start = dictionary["SPO2Start"] as? Int
        measurmentidentifier.systolicStart = dictionary["BP_SystolicStart"] as? Int
        measurmentidentifier.systolicEnd = dictionary["BP_SystolicEnd"] as? Int
        measurmentidentifier.diastolicStart = dictionary["BP_DistolicStart"] as? Int
        measurmentidentifier.diastolicEnd = dictionary["BP_DistolicEnd"] as? Int
        measurmentidentifier.bgStart = dictionary["BloodGlucoseStart"] as? Double
        measurmentidentifier.heartrateEnd = dictionary["HeartRateEnd"] as? Int
        measurmentidentifier.heartRateStart = dictionary["HeartRateStart"] as? Int
        measurmentidentifier.bgEnd = dictionary["BloodGlucoseEnd"] as? Double
        measurmentidentifier.InitialInstructionsHeading = dictionary["InitialInstructionsHeading"] as? String
        measurmentidentifier.InitialInstructions = dictionary["InitialInstructions"] as? String
        measurmentidentifier.IsMessagingAllowed = dictionary["IsMessagingAllowed"] as! Bool
        measurmentidentifier.temperatueEnd = dictionary["TemperatureEnd"] as? Double
        measurmentidentifier.temperatureStart = dictionary["TemperatureStart"] as? Double
    }
    
    func savemessages(array:[Message])
    {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "UserMessage", in: managedContext)!
        
        
        
    }
    
    func saveMeasurementType(array:[Measurement])  {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "MeasurementType", in: managedContext)!
        
        let measurmentidfier = MeasurementIdentifier.sharedInteractor
        
        for measurement in array{
            
            let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
//            dataObject.setValue(measurement.Name, forKeyPath: "name")
//
//            dataObject.setValue(measurement.id, forKeyPath: "id")
//            dataObject.setValue(measurement.Unit, forKeyPath: "unit")
//
            if (measurement.Name == "HeartRate"){
                let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
                dataObject.setValue(measurement.Name, forKeyPath: "name")

                dataObject.setValue(measurmentidfier.pulseID, forKeyPath: "id")
                dataObject.setValue(measurement.Unit, forKeyPath: "unit")

            }else if (measurement.Name == "Weight"){

                if measurmentidfier.weightID == measurement.id{
                    let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
                    dataObject.setValue(measurement.Name, forKeyPath: "name")
                    dataObject.setValue(measurmentidfier.weightID, forKeyPath: "id")

                    dataObject.setValue(measurement.Unit, forKeyPath: "unit")
                }
            }else if (measurement.Name == "BloodGlucose"){
                if measurmentidfier.bloodsugar == measurement.id{
                    let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
                    dataObject.setValue(measurement.Name, forKeyPath: "name")

                    dataObject.setValue(measurement.Unit, forKeyPath: "unit")
                    dataObject.setValue(measurmentidfier.bloodsugar, forKeyPath: "id")

                }
            }else if (measurement.Name == "Temperature"){
                if measurmentidfier.temperatureID == measurement.id{
                    let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
                    dataObject.setValue(measurement.Name, forKeyPath: "name")

                    dataObject.setValue(measurement.Unit, forKeyPath: "unit")
                    dataObject.setValue(measurmentidfier.temperatureID, forKeyPath: "id")

                }

            }else if (measurement.Name == "SPO2"){
                let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
                dataObject.setValue(measurement.Name, forKeyPath: "name")

                dataObject.setValue(measurmentidfier.spo2ID, forKeyPath: "id")
                dataObject.setValue(measurement.Unit, forKeyPath: "unit")

            }else if (measurement.Name == "BloodPressure"){
                let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
                dataObject.setValue(measurement.Name, forKeyPath: "name")

                dataObject.setValue(measurmentidfier.systolicID, forKeyPath: "id")
                dataObject.setValue(measurement.Unit, forKeyPath: "unit")

            }
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
        }
    }
    
    
    func saveMeasurement(tablename:String, measurement : MeasurementData)  {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: tablename, in: managedContext)!
        

            let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
            dataObject.setValue(measurement.value, forKeyPath: "value")
            dataObject.setValue(measurement.measurementDateTime, forKeyPath: "time")
            dataObject.setValue(measurement.measurementUnit, forKeyPath: "unit")
            dataObject.setValue(measurement.linkedId, forKey: "linkedID")
     
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
    }
    
    func getMeasurement(tablename:String) -> [MeasurementData]  {
        var measurementarray = [MeasurementData]()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return measurementarray
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: tablename)
        let dateSort:NSSortDescriptor = NSSortDescriptor(key:"time", ascending: false, selector: #selector(NSString.localizedStandardCompare))
        request.sortDescriptors = [dateSort]
        
        do{
            let result = try managedContext.fetch(request)
            for data in result as! [NSManagedObject] {
                var measurement = MeasurementData()
                measurement.value = " "
                measurement.measurementDateTime = " " //not a good way. change to init later on
                measurement.value = data.value(forKey: "value") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
                measurement.measurementDateTime = data.value(forKey: "time") as? String
                measurement.linkedId = data.value(forKey: "linkedID") as? String
                print(data.value(forKey: "linkedID"))
                measurementarray.append(measurement)
            }
        } catch{
            print("failed to fetch data from db")
        }

        
        return measurementarray
    }
    
    func getUserDetails() -> UserDetails {
        var userDetails = UserDetails()
        userDetails.firstName = " "
        userDetails.lastName = " "
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                userDetails.firstName = data.value(forKey: "FirstName") as? String
                userDetails.lastName = data.value(forKey: "LastName") as? String
                userDetails.patientId = data.value(forKey: "id") as? String
                userDetails.lastLogin = data.value(forKey: "lastLogin") as? String
            }
        } catch {
            print("Failed")
        }
        
        return userDetails;
    }
        
    func getLatestMeasurement(tableName:String) -> DashboardData?{
        var measurementArray = [CGFloat]()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: tableName)
        
        fetchRequest.returnsObjectsAsFaults = false
        let dateSort:NSSortDescriptor = NSSortDescriptor(key:"time", ascending: true, selector: #selector(NSString.localizedStandardCompare))
        fetchRequest.sortDescriptors = [dateSort]
        
        do{
            let result = try managedContext.fetch(fetchRequest)
            var data1 : [CGFloat] = []
            var data2 : [CGFloat] = []
            
            for data in result as! [NSManagedObject] {
                let data = data.value(forKey: "value")  as! NSString
                if(tableName == "BloodPressure" || tableName == "Pulse"){
                    let dataValue: [String] =  (data.components(separatedBy: "/"))
                    data1.append(CGFloat(NumberFormatter().number(from: dataValue[0])!))
                    if dataValue.count > 1 && dataValue[1] != ""{
                    data2.append(CGFloat(NumberFormatter().number(from: dataValue[1])!))
                    }
                }else{
                    measurementArray.append(CGFloat((data).doubleValue))
                    data1.append(CGFloat((data).doubleValue))
                }
            }
            return DashboardData(measurementArray: data1, secondaryMeasurementArray: data2)
        } catch{
            print("failed to fetch data from db")
        }
        return nil
    }
    
    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    
    func getMeasurementType(measurementTypeId:String) ->MeasurementType!{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return MeasurementType()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
 //       fetchRequest.predicate = NSPredicate(format: "id == %@",measurementTypeId)
        do{
            let result = try? managedContext.fetch(fetchRequest)
            for data in result as! [MeasurementType] {
                let data = data as! MeasurementType
                if data.id == measurementTypeId
                {
                    return data
                }
            }
        } catch{
            print("failed to fetch data from db")
        }
        
        return  nil
        
        
    }
    
    /**
     No update of isCompleted and isStarted flag values while parseing nas storing from server
     */
    func saveSessionFromServer(sessionId: String,assessmentId:String,date:Date?, name: String? = nil ,patientId: String? = nil,startTime: Date?,endTime: Date?,assessmentLastUpdated: Date?)  {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,assessmentIdPredicate])
        
        let result = try? managedContext.fetch(fetchRequest)
        let resultData = result as! [Assessment]
        
        if(resultData.count>0){
            
            for object in resultData {

                if((assessmentLastUpdated! <= object.assessmentLastUpdated!) == false){

                    let assessment = Assessment(sessionId: sessionId, assessmentId:assessmentId, name:name,date:date, startTime: startTime, endTime: endTime, assessmentLastUpdated: assessmentLastUpdated,answeredDateTime:nil, insertIntoManagedObjectContext: managedContext)
                    do {
                        try managedContext.save()
                        NetworkManager().getAssessmentNodes(assessmentId: assessmentId, sessionId:sessionId,  completion:{ _ in })
                    } catch let error as NSError {
                        print("Could not save updated from server. \(error), \(error.userInfo)")
                    }
                    //endof if checck of date of assessment
                }
                
                
            }
            //endof if check for resultset count
        }
            
        else {
            
            print("#@#-NEW-\(sessionId)")
            //assessment not available offline
            let assessment = Assessment(sessionId: sessionId, assessmentId:assessmentId, name:name,date:date, startTime: startTime, endTime: endTime, assessmentLastUpdated: assessmentLastUpdated,answeredDateTime:nil, insertIntoManagedObjectContext: managedContext)
            do {
                try managedContext.save()
                NetworkManager().getAssessmentNodes(assessmentId: assessmentId, sessionId:sessionId, completion:{ _ in })
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        
    }
    
    
    
    func markSessionAsComplete(sessionId: String,assessmentId:String){
        let  update = {
            
            (context: NSManagedObjectContext) in
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
            let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
            let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
            // Get the current calendar with local time zone
            var calendar = Calendar.current
            let dateValue = calendar.startOfDay(for: Date()) // eg. 2016-10-10 00:00:00
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,assessmentIdPredicate,/*datePredicate*/])
            
            let result = try? context.fetch(fetchRequest)
            let resultData = result as! [Assessment]
            
            if(resultData.count>0){
                for object in resultData {
                    //
                    var contextNode = object
                    contextNode.isCompleted = true
                    do {
                        try context.save()
                    } catch let error as NSError {
                        print("Could not save update session status. \(error), \(error.userInfo)")
                    }
                }
            }
            
            OperationQueue.main.addOperation {
                //            completion()
            }
            
        }
        /*
         Perform operation
         */
        let operation = PersistenceOperation(update)
        operation.name = "markSessionAsComplete"
        isolationQueue.addOperation(operation)
        
    }
    
    
    /**
     Update the isSubmitted flag of each session(assessment).
     Calls getAssessments && getUntakenChildNodesOfAssessmentParent internally
     - Parameters:
     - patientId : Patient whose session is send
     - sessionId : The session being send
     - assessmentId: The assessment of the session being send.
     
     - Throws: .
     
     - Returns: none.
     */
    func markSessionAsSubmitted(sessionId: String,assessmentId:String){
        let  update = {
            
            (context: NSManagedObjectContext) in
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
            let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
            let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
            // Get the current calendar with local time zone
            var calendar = Calendar.current
            calendar.timeZone = NSTimeZone.local
            let dateValue = calendar.startOfDay(for: Date()) // eg. 2016-10-10 00:00:00
            let datePredicate = NSPredicate(format: "date == %@",dateValue as NSDate)
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,assessmentIdPredicate,datePredicate])
            
            let result = try? context.fetch(fetchRequest)
            let resultData = result as! [Assessment]
            
            if(resultData.count>0){
                for object in resultData {
                    //
                    let contextNode = object
                    contextNode.isSubmitted = true
                    do {
                        try context.save()
                    } catch let error as NSError {
                        print("Could not save update session status. \(error), \(error.userInfo)")
                    }
                }
            }
            
            OperationQueue.main.addOperation {

            }
            
        }
        /*
         Perform operation
         */
        let operation = PersistenceOperation(update)
        operation.name = "markSessionAsSubmitted"
        isolationQueue.addOperation(operation)
        
    }
    
    
    
    func getPendingAssessments() -> [Assessment]
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Assessment]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
       
        
        let isNotCompletedPredicateNil = NSPredicate(format: "isCompleted == nil")
        let isNotCompletedPredicateZero = NSPredicate(format: "isCompleted == 0")
        
        let isNotCompletedPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [isNotCompletedPredicateNil,isNotCompletedPredicateZero])
        
        let isNotSubmittedPredicateNil = NSPredicate(format: "isSubmitted == nil")
        let isNotSubmittedPredicateZero = NSPredicate(format: "isSubmitted == 0")
        
        let isNotSubmittedPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [isNotSubmittedPredicateNil,isNotSubmittedPredicateZero])
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [isNotCompletedPredicate,isNotSubmittedPredicate])
//        let orderSort = NSSortDescriptor(key: "date", ascending:true)
//        request.sortDescriptors = [orderSort]
        do{
            let result = try managedContext.fetch(request)
        
            return result as! [Assessment]
        } catch{
            print("failed to fetch assessment data from db")
        }
        return [Assessment]()

    }
    
    func getAssessments() -> [Assessment]{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Assessment]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
        let orderSort = NSSortDescriptor(key: "date", ascending:true)
        request.sortDescriptors = [orderSort]
        
        do{
            let result = try managedContext.fetch(request)
            
            return result as! [Assessment]
        } catch{
            print("failed to fetch assessment data from db")
        }
        
        return [Assessment]()
        
    }
    
    /**
     Updated method to list the assessment whose date of end is today or a later date.
     - Parameters:
     
     - Throws: .
     
     - Returns: [Assessments].
     */
    func getAssessmentsFromToday() -> [Assessment]{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Assessment]()
        }
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        let dateFrom = calendar.startOfDay(for: Date()) // eg. 2016-10-10 00:00:00
        //1 week from today
        let dateTo = calendar.date(byAdding: .day, value: 7, to: dateFrom)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
        
        
        let fromPredicate = NSPredicate(format: "startTime >= %@",dateFrom as NSDate)
        let toPredicate = NSPredicate(format: "endTime < %@", dateTo as! NSDate)


        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [fromPredicate,toPredicate])
        
        do{
            let result = try managedContext.fetch(request)
            
            return result as! [Assessment]
        } catch{
            print("failed to fetch assessment data from db")
        }
        
        return [Assessment]()
        
    }
    
    
    /**
     Get the completed assessments
     - Parameters:
     
     - Throws: .
     
     - Returns: [Assessments].
     */
    func getCompletedAssessments(completion:@escaping (_ completedAssessments:  [Assessment])->(Void)){
        
        let  fetch = {
            
            (context: NSManagedObjectContext) in
            
            var result:[Assessment] = []
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
            
            let isCompletedPredicate = NSPredicate(format: "isCompleted == 1")
            
            let isSubmittedPredicateZero = NSPredicate(format: "isSubmitted == 0")
            let isSubmittedPredicateNil = NSPredicate(format: "isSubmitted == nil")
            
            let isSubmittedPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [isSubmittedPredicateZero,isSubmittedPredicateNil])
            
            
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [isCompletedPredicate,isSubmittedPredicate])
            
            do{
                let results = try context.fetch(fetchRequest)
                
                result = results as! [Assessment]
                print("Completed assessments=\(result)")
            } catch{
                print("failed to fetch assessment data from db")
            }
            /*
             Finalize
             */
            OperationQueue.main.addOperation {
                completion(result)
            }
            
        }
        /*
         Perform operation
         */
        let operation = PersistenceOperation(fetch)
        operation.name = "fetchCompletedSessions"
        isolationQueue.addOperation(operation)
        
    }
    
    /**
     No update of isCompleted and isStarted flag values while parseing nas storing from server
     */
    func saveNodeFromServer(id: String,assessmentId: String,sessionId:String, text:String? = nil,value:String? = nil,nodeType:Node.NodeType, alertType:Node.AlertType, parentId:String? = nil,measurementTypeId:String? = nil,options:[String]? = nil,children:[String]? = nil)  {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        let nodeIdPredicate = NSPredicate(format: "id == %@",id)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,nodeIdPredicate])
        
        let result = try? managedContext.fetch(fetchRequest)
        let resultData = result as! [Node]
        if resultData.count != 0{
            //node available offline
            var contextNode = resultData[0]
            contextNode.assessmentId = assessmentId
            contextNode.sessionId = sessionId
            contextNode.text = text
            contextNode.nodeType = nodeType
            contextNode.alertType = alertType
            contextNode.parentId = parentId
            contextNode.measurementTypeId = measurementTypeId
            contextNode.options = options
            contextNode.children = children
//            print("UPDATED:\(contextNode.id)----\(contextNode)")
        }
        else{
            //node not available offline
            let node = Node(id: id, assessmentId: assessmentId, sessionId: sessionId, text: text, answerId: nil, value: value, nodeType: nodeType.rawValue, alertType: alertType.rawValue, parentId: parentId, measurementTypeId:measurementTypeId, measurementDateTime: nil, options: options, children: children, isTaken: false, isCompleted: false, insertIntoManagedObjectContext: managedContext)
//            print("CREATED:\(id)----\(node)")
        }
        
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
     func updateNode(updatedNodeId:String,sessionId:String,updatedValue:String?,answerId:String?,measurementDateTime:Date?,isTaken:Bool,isCompleted:Bool,measurementTypeId:String? = "" ,measurementValueTypeId:String? = "" ,measurementValue:String? = ""){

        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        let nodeIdPredicate = NSPredicate(format: "id == %@",updatedNodeId)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,nodeIdPredicate])
        
        let result = try? managedContext.fetch(fetchRequest)
        let resultData = result as! [Node]
        if resultData.count != 0{
            var contextNode = resultData[0]
            contextNode.value = updatedValue
            contextNode.answerId = answerId
            contextNode.measurementDateTime = measurementDateTime
            contextNode.measurementTypeId = measurementTypeId
            contextNode.measurementValueTypeId = measurementValueTypeId
            contextNode.measurementValue = measurementValue
            contextNode.isTaken = isTaken
            contextNode.isCompleted = isCompleted
            do {
                try managedContext.save()
                
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        
    }
    
    
    func updateNodeTaken(updatedNodeId:String,sessionId:String,isTaken:Bool){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        let nodeIdPredicate = NSPredicate(format: "id == %@",updatedNodeId)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,nodeIdPredicate])
        
        let result = try? managedContext.fetch(fetchRequest)
        let resultData = result as! [Node]
        if resultData.count != 0{
            var contextNode = resultData[0]
            contextNode.isTaken = isTaken
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        print("UPDATED NODE TAKEN---",CoreDataUtils().getNode(id:updatedNodeId))
        
    }
    
    func deleteAllNode(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There was an error")
        }
        
    }
    
    
    func getNodesOfAssessmentParent2(assessmentId:String,parentId:String) ->[Node]{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Node]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        //predicates
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
        let parentIdPredicate = NSPredicate(format: "parentId == %@",parentId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [parentIdPredicate,assessmentIdPredicate])
        //sortDescriptors
        let orderSort = NSSortDescriptor(key: #keyPath(Node.orderNumber), ascending: true)
        fetchRequest.sortDescriptors = [orderSort]
        do{
            let result = try? managedContext.fetch(fetchRequest)
            return result as! [Node]
        } catch{
            print("failed to fetch data from db")
        }
        
        return  [Node]()
    }
    
    //the question with isTAken = 0 are fetched, ordered and the first one is fetched as the next question
    func getNextQuestion(assessmentID:String, sessionID:String, parentID:String)->[Node]{
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Node]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        //predicates
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentID)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionID)
        let parentIdPredicate = NSPredicate(format: "parentId == %@",parentID)
        let isNotTakenPredicate = NSPredicate(format: "isTaken == %d",0)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [parentIdPredicate,sessionIdPredicate,assessmentIdPredicate,isNotTakenPredicate])
        //sortDescriptors
        let orderSort = NSSortDescriptor(key: #keyPath(Node.orderNumber), ascending: true)
        fetchRequest.sortDescriptors = [orderSort]
        do{
            let result = try? managedContext.fetch(fetchRequest)
            return result as! [Node]
        } catch{
            print("failed to fetch data from db")
        }
        
        
    }
    
    func getUntakenChildNodesOfAssessmentParent(assessmentId:String,sessionId:String,parentId:String) ->[Node]{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Node]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        //predicates
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        let parentIdPredicate = NSPredicate(format: "parentId == %@",parentId)
        let isNotTakenPredicate = NSPredicate(format: "isTaken == %d",0)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [parentIdPredicate,sessionIdPredicate,assessmentIdPredicate,isNotTakenPredicate])
        //sortDescriptors
        let orderSort = NSSortDescriptor(key: #keyPath(Node.orderNumber), ascending: true)
        fetchRequest.sortDescriptors = [orderSort]
        do{
            let result = try? managedContext.fetch(fetchRequest)
            return result as! [Node]
        } catch{
            print("failed to fetch data from db")
        }
        
    }
    
    /**
     Similar in funcatiionality with getUntakenChildNodesOfAssessmentParent().
     Written as separate method to avoid mixup of functionalities
     
     - Parameters:
     - patientId : Patient whose session is send
     - sessionId : The session being send
     - assessmentId: The assessment of the session being send.
     
     - Throws: .
     
     - Returns: [Node].
     */
    func getTakenChildNodesOfAssessmentParent(assessmentId:String,sessionId:String,parentId:String) ->[Node]{

        var result:[Node] = []

        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Node]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        //predicates
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        let parentIdPredicate = NSPredicate(format: "parentId == %@",parentId)
        let isNotTakenPredicate = NSPredicate(format: "isTaken == %d",1)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [parentIdPredicate,sessionIdPredicate,assessmentIdPredicate,isNotTakenPredicate])
        //sortDescriptors
        let orderSort = NSSortDescriptor(key: #keyPath(Node.orderNumber), ascending: true)
        fetchRequest.sortDescriptors = [orderSort]
                
        do{
            let results = try? managedContext.fetch(fetchRequest)
            result =  results as! [Node]
            return result
        } catch{
            print("failed to fetch data from db")
        }

        
        return  [Node]()
        
    }
    
    
    func getTakenCompletedChildNodesOfAssessmentParent(assessmentId:String,sessionId:String,parentId:String) ->[Node]{
        
        var result:[Node] = []
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Node]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        //predicates
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        let parentIdPredicate = NSPredicate(format: "parentId == %@",parentId)
        let isNotTakenPredicate = NSPredicate(format: "isTaken == %d",1)
        let isCompletedPredicate = NSPredicate(format: "isCompleted == %d",1)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [parentIdPredicate,sessionIdPredicate,assessmentIdPredicate,isNotTakenPredicate,isCompletedPredicate])
        //sortDescriptors
        let orderSort = NSSortDescriptor(key: #keyPath(Node.orderNumber), ascending: true)
        fetchRequest.sortDescriptors = [orderSort]
        
        do{
            let results = try? managedContext.fetch(fetchRequest)
            result =  results as! [Node]
            return result
        } catch{
            print("failed to fetch data from db")
        }
        return  [Node]()
        
    }
    
    func getNode(id:String) ->Node!{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return Node()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        fetchRequest.predicate = NSPredicate(format: "id == %@",id)
        do{
            let result = try? managedContext.fetch(fetchRequest)
            for data in result as! [Node] {
                return data
            }
        } catch{
            print("failed to fetch data from db")
        }
        
        return  nil
        
        
    }
    
    
    
    func getNodesOfAssessmentSession(assessmentId:String,sessionId:String) ->[Node]{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return [Node]()
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        //predicates
        let assessmentIdPredicate = NSPredicate(format: "assessmentId == %@",assessmentId)
        let parentIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [parentIdPredicate,assessmentIdPredicate])
        //sortDescriptors
        let orderSort = NSSortDescriptor(key: #keyPath(Node.orderNumber), ascending: true)
        fetchRequest.sortDescriptors = [orderSort]
        do{
            let result = try? managedContext.fetch(fetchRequest)
            return result as! [Node]
        } catch{
            print("failed to fetch data from db")
        }
        
        return  [Node]()
        
        
    }
    
    func getCompletedAssessmentCount() -> Int{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return 0
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
        let orderSort = NSSortDescriptor(key: "date", ascending:true)
        request.sortDescriptors = [orderSort]
        
        let isCompletedPredicate = NSPredicate(format: "isCompleted == 1")
        
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [isCompletedPredicate,/*isSubmittedPredicate*/])
        
        do{
            let result = try managedContext.fetch(request)
            
            return result.count
        } catch{
            print("failed to fetch assessment data from db")
        }
        
        return 0;
        
        
    }
    
    func getMissedAssessmentsCount() -> Int{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return 0
        }
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        let dateToday = calendar.startOfDay(for: Date()) // eg. 2016-10-10 00:00:00
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
        
        
        let fromPredicate = NSPredicate(format: "endTime <= %@",NSDate())
        let isNotCompletedPredicateNil = NSPredicate(format: "isCompleted == nil")
        let isNotCompletedPredicateZero = NSPredicate(format: "isCompleted == 0")
        
        let isNotCompletedOrPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [isNotCompletedPredicateNil,isNotCompletedPredicateZero])
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [fromPredicate,isNotCompletedOrPredicate])
        
        do{
            let result = try managedContext.fetch(request)
            
            return result.count
        } catch{
            print("failed to fetch assessment data from db")
        }
        
        return 0
        
    }
    
    func getActiveAssessmentsCount() -> Int{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return 0
        }
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
//        let dateToday = calendar.startOfDay(for: Date())
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assessment")
        let startPredicate = NSPredicate(format: "startTime <= %@",NSDate())
        let endPredicate = NSPredicate(format: "endTime >= %@",NSDate())
        let isNotCompletedPredicateNil = NSPredicate(format: "isCompleted == nil")
        let isNotCompletedPredicateZero = NSPredicate(format: "isCompleted == 0")
        
        let isNotCompletedOrPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [isNotCompletedPredicateNil,isNotCompletedPredicateZero])
        
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [startPredicate,endPredicate,isNotCompletedOrPredicate])
        
        do{
            let result = try managedContext.fetch(request)
            
            return result.count
        } catch{
            print("failed to fetch assessment data from db")
        }
        
        return 0
        
    }
    
    func deleteAllData(_ entity:String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        do {
            let results = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                appDelegate.persistentContainer.viewContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    
    func savemeasurements(dictionary:[String:Any])
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        var measurementName = dictionary["MeasurementName"] as! String
        let managedContext = appDelegate.persistentContainer.viewContext
        if measurementName == "SPO1"{
            measurementName = "SPO2"
        }
        if measurementName == "HeartRate"{
            measurementName = "Pulse"
        }
        
        let entity = NSEntityDescription.entity(forEntityName: measurementName, in: managedContext)!
        let dataObject = NSManagedObject(entity: entity, insertInto: managedContext)
        
        dataObject.setValue(dictionary["MeasurementUnit"], forKeyPath: "unit")
        dataObject.setValue(dictionary["Value"], forKeyPath: "value")
        dataObject.setValue(dictionary["LinkedMeasurementId"], forKey: "linkedID")
        dataObject.setValue(dictionary["MeasurementDateTime"], forKey: "time")
        
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveOfflineMeasurement(tablename:String, offlineTableName:String,  measurement : MeasurementData)  {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        // let entity = NSEntityDescription.entity(forEntityName: tablename, in: managedContext)!
        let anyObject = NSEntityDescription.insertNewObject(forEntityName: "RPMOfflineModel", into: managedContext) as? RPMOfflineModel
        
        anyObject?.tableName = offlineTableName
        
        //let measure = measurement as NSObject
        var jsonString : String?
        
        do {
            let jsonData = try JSONEncoder().encode(measurement)
            jsonString = String(data: jsonData, encoding: .utf8)!
            print(jsonString) // [{"sentence":"Hello world","lang":"en"},{"sentence":"Hallo Welt","lang":"de"}]
            
            // and decode it back
            let decodedSentences = try JSONDecoder().decode(MeasurementData.self, from: jsonData)
            print(decodedSentences)
        } catch { print(error) }
        
        anyObject?.offlineMeasurementData = jsonString
        
        do {
            try managedContext.save()
           
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    func getAllOfflineSavedData(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "RPMOfflineModel")
        
        
        do{
            let result = try managedContext.fetch(request)
            let group = DispatchGroup()
            for data in result as! [RPMOfflineModel] {
                group.enter()
                let jsonData = data.offlineMeasurementData?.data(using: .utf8)!
                let measurementData = try JSONDecoder().decode(MeasurementData.self, from: jsonData!)
                self.uploadOfflinedata(measurement: measurementData)
                group.leave()
                //Write function push data server
                
            }
        } catch{
            print("failed to fetch data from db")
        }
    }
    
    func uploadOfflinedata(measurement: MeasurementData){
        var meaurementData = measurement
        if measurement.measurementValueType == "2/3" {
            let arr = measurement.value?.components(separatedBy: "/")
            
            meaurementData.value = (arr?[0])! + "/" + (arr?[1])!
            
        }
        
        NetworkManager().uploadData(measurementData: meaurementData ){ (response) -> () in
        
            print("Veeresh")
        }
    }
    
    func appendMeasurements() {
       
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        var BPmeasurementarray = [BloodPressure]()
        var HRmeasurementarray = [Pulse]()
     
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestBP = NSFetchRequest<NSFetchRequestResult>(entityName: "BloodPressure")
        
        do{
           // let result = try managedContext.fetch(request)
            let results = try? appDelegate.persistentContainer.viewContext.fetch(fetchRequestBP)
            for data in results as! [NSManagedObject] {
                BPmeasurementarray.append(data as! BloodPressure)
            }
        } catch {
            print("failed to fetch data from db")
        }
        
        let fetchRequestHR = NSFetchRequest<NSFetchRequestResult>(entityName: "Pulse")
        do{
            // let result = try managedContext.fetch(request)
            let results = try? appDelegate.persistentContainer.viewContext.fetch(fetchRequestHR)
            for data in results as! [NSManagedObject] {
                HRmeasurementarray.append(data as! Pulse)
            }
        } catch{
            print(Error.self)
        }

        for each in BPmeasurementarray {
            
            for object in HRmeasurementarray{
                if (each.linkedID == object.linkedID) {
                   // print(each.value! + "/" + object.value!)
                    each.value = (each.value! + "/" + object.value!)
                    break
                }
            }
        }
       
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
 
        ////////////////////////////////////////////////////////////////////////////////////
        //let result = try? managedContext.fetch(fetchRequest)
     //   let resultData = result as! [BloodPressure]
        
        // get BP table seperately ()
        //entity 1 = BP    entity 2 = pulse
        
    }
    
    func appendMeasurementsPulse() {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        var SPO2measurementarray = [SPO2]()
        var HRmeasurementarray = [Pulse]()
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestBP = NSFetchRequest<NSFetchRequestResult>(entityName: "SPO2")
        
        do{
            // let result = try managedContext.fetch(request)
            let results = try? appDelegate.persistentContainer.viewContext.fetch(fetchRequestBP)
            for data in results as! [NSManagedObject] {
                SPO2measurementarray.append(data as! SPO2)
            }
        } catch {
            print("failed to fetch data from db")
        }
        
        let fetchRequestHR = NSFetchRequest<NSFetchRequestResult>(entityName: "Pulse")
        do{
            // let result = try managedContext.fetch(request)
            let results = try? appDelegate.persistentContainer.viewContext.fetch(fetchRequestHR)
            for data in results as! [NSManagedObject] {
                HRmeasurementarray.append(data as! Pulse)
            }
        } catch{
            print(Error.self)
        }
        
        for each in HRmeasurementarray {
            
            for object in SPO2measurementarray{
                if (each.linkedID == object.linkedID) {
                    print(each.value! + "/" + object.value!)
                    each.value = (each.value! + "/" + object.value!)
                    break
                }
            }
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        ////////////////////////////////////////////////////////////////////////////////////
        //let result = try? managedContext.fetch(fetchRequest)
        //   let resultData = result as! [BloodPressure]
        
        // get BP table seperately ()
        //entity 1 = BP    entity 2 = pulse
        
    }
    
    func updateNodeTakenCompleted(updatedNodeId:String,sessionId:String,isTaken:Bool,isCompleted:Bool){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Node")
        let nodeIdPredicate = NSPredicate(format: "id == %@",updatedNodeId)
        let sessionIdPredicate = NSPredicate(format: "sessionId == %@",sessionId)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionIdPredicate,nodeIdPredicate])
        
        let result = try? managedContext.fetch(fetchRequest)
        let resultData = result as! [Node]
        if resultData.count != 0{
            var contextNode = resultData[0]
            contextNode.isTaken = isTaken
            contextNode.isCompleted = isCompleted
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        
        print("UPDATED NODE COMPLETED---",CoreDataUtils().getNode(id:updatedNodeId))
        
    }
    
    
    func deleteAllDatabase() {
        deleteAllData("Assessment")
        deleteAllData("BloodGlucose")
        deleteAllData("BloodPressure")
        deleteAllData("Customer")
        deleteAllData("MeasurementType")
        deleteAllData("MeasurementValueType")
        deleteAllData("OfflineDataTable")
        deleteAllData("Pulse")
        deleteAllData("RPMOfflineModel")
        deleteAllData("Node")
        deleteAllData("SPO2")
        deleteAllData("Temperature")
        deleteAllData("User")
        deleteAllData("Weight")
        
        MeasurementIdentifier.sharedInteractor.messages = []
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.removeObject(forKey: Constants.ACCESS_TOKEN_DEFAULT)
        UserDefaults.standard.removeObject(forKey: Constants.ACCESS_TOKEN_TOEKN_EXPIRY)
        UserDefaults.standard.removeObject(forKey: Constants.USER_DEF_LOGIN_STATUS)
        UserDefaults.standard.removeObject(forKey: Constants.USER_DEF_PATIENT_ID)
        UserDefaults.standard.removeObject(forKey: "LoggedIn")
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
}
