//
//  RPMSignatureTableViewCell.swift
//  Sample
//
//  Created by EHC_Bosch on 10/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMSignatureTableViewCell: UITableViewCell,RPMSignaturePopoverDelegate {

    @IBOutlet var sigImage : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func didSendSignature(signatureImage : UIImage, popup:RPMSignaturePopup){
        // self.sigView.clear()
        sigImage.image = signatureImage
        //AppointmentDetails.sharedappointmentDetails.signature = signatureImage
        //self.redo.isHidden = false
        popup.fadeOut()
    }
    
    func showSigAlert(title: String, message: String) {
        //
    }
}
