//
//  AppDelegate.swift
//  Sample
//
//  Created by Isham on 27/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var token = ""
    var offlineSyncNeeded : Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let notiicationDelegate = VideoConferenceNotificationDelegate()
        UNUserNotificationCenter.current().delegate = notiicationDelegate
            
        registerForPushNotifications()
        return true
    }
    
    @objc func reachabilityChanged(note: Notification) {
        do {
            NetworkManager().startObservingforNetworkReachability()
        }
    }
    func application(
        _ application:UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        guard let aps = userInfo["aps"] as? [String :AnyObject] else {
            completionHandler(.failed)
            return
        }
        
        let text = aps["alert"] as! String
        let meetingNumber = text.split(separator: "_")[1]
        if CoreDataUtils().getUserDetails().patientId != nil{
        self.presentZoomVideo(String(meetingNumber))
        }
    }
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        token = tokenParts.joined()
        print("Device Token: \(token)")
    }
    
    func returndevicetoken() -> String {
        return self.token
    }
    
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Sample")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func registerForPushNotifications(){
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert,.sound,.badge]) {
            [weak self] granted,error in
            print("push permission granted \(granted)")
            guard granted else { return }
            
            let attendAction = UNNotificationAction(identifier: "Attend", title: "Attend", options: [.foreground])
            let rejectAction = UNNotificationAction(identifier: "Reject", title: "Reject", options: [.foreground])
            
            let callCategory = UNNotificationCategory(
                identifier: "CALL", actions: [attendAction,rejectAction],
                intentIdentifiers: [], options: [])
            center.setNotificationCategories([callCategory])
            
            self?.getNotificationSettings()
        }
    }
    
    func getNotificationSettings(){
        UNUserNotificationCenter.current().getNotificationSettings{
            settings in print("Push notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func makeASelfPhoneCall(){
        let url:NSURL = URL(string:"" )! as NSURL
        //        UIApplication.shared.canOpenURL(url){
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL,options: [:],completionHandler: nil)
        }
        else{
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    func presentZoomVideo(_ meetingNumber:String){
        
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewController = storyBoard.instantiateViewController(withIdentifier: "VideoConferenceViewController") as! VideoConferenceViewController
//        newViewController.rtc_meeting = meetingNumber
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        self.window?.rootViewController = newViewController
//        self.window?.makeKeyAndVisible()
//        print("%%%%%%% Window Opened %%%%%%%")
    }
    

    
    //notification action delegate
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
    
    
    @available(iOS 10.0,*)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier{
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("CAll Action")
          //  presentZoomVideo()
        case "Attend":
            print("Attend")
          //  presentZoomVideo()
        case "Reject":
            print("Reject")
        default:
            print("Default")
        }
        
        
        completionHandler()
    }
    
}

