//
//  VitalDashButton.swift
//  Sample
//
//  Created by Thejus Jose on 25/05/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class VitalDashButton: UICollectionViewCell {

    @IBOutlet var buttonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(text:String){
        self.buttonLabel.text = text
    }

    func getData()-> String?{
        return self.buttonLabel.text
    }
}
