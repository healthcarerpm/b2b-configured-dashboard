//
//  PatientVitalLink.swift
//  Sample
//
//  Created by Thejus Jose on 27/05/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation

struct PatientVitalLink:Codable {
    
    var Id: String?
    var PatientId : String?
    var MeasurementTypeId : String?
    var MeasurementType : String?
    var AssignedDate: String?
    var RemoveDate : String?
    var LastActionBy: String?
    var IsActive : Bool?
}
