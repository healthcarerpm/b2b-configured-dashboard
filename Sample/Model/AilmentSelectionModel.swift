//
//  AilmentSelectionModel.swift
//  Sample
//
//  Created by Admin on 1/14/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation


class SelectImages
{
    var title: String = ""
    var isSelected: Bool = false
    var id = 0
    var selectedImgs = [String]()
    
    func addImgToCollection(title:String)
    {
        selectedImgs.append(title)
    }
    
    func removeFromSelection(title: String)
    {
        selectedImgs.removeAll { (objectFromArray) -> Bool in
            objectFromArray == title
        }
    }
    
}
