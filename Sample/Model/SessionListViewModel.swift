//
//  SessionListViewModel.swift
//  Sample
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

extension SessionListViewController {
    
    class ViewModel {
        
        private var assessments: [Assessment]
        private lazy var dateFormatter: DateFormatter = {
            let fmtr = DateFormatter()
            fmtr.dateFormat = "EEEE, MMM d"
            return fmtr
        }()
        
        var numberOfAssessments: Int {
            return assessments.count
        }
        
        private func assessment(at index: Int) -> Assessment {
            return assessments[index]
        }
        
        func text(at index: Int) -> String {
            return assessment(at: index).name ?? ""
        }
        
        func sessionId(at index: Int) -> String {
            return assessment(at: index).sessionId
        }
        
        func assessmentId(at index: Int) -> String {
            return assessment(at: index).assessmentId
        }
        
        func isSessionCompleted(at index:Int) -> Bool{
            return assessment(at: index).isCompleted
        }
        
        func isSessionSubmitted(at index:Int) -> Bool{
            return assessment(at: index).isSubmitted
        }
        
        func date(at index: Int) -> String{
            guard let startDate = self.assessment(at: index).startTime else{
                return ""
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MMM/yyyy"
            let result = dateFormatter.string(from: startDate)
            return result
        }
        
        
        func startTime(at index: Int) -> String{
            guard let startDate = self.assessment(at: index).startTime else{
                return ""
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let result = dateFormatter.string(from: startDate)
            return result
            
            
        }
        
        func startDateTime(at index: Int) -> Date{
            guard let startDate = self.assessment(at: index).startTime else{
                return Date()
            }
            return startDate
        }
        
        func endTime(at index: Int) -> String{
            guard let endDate = self.assessment(at: index).endTime else{
                return ""
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let result = dateFormatter.string(from: endDate)
            return result
        }
        
        func endDateTime(at index: Int) -> Date{
            guard let endDate = self.assessment(at: index).endTime else{
                return Date()
            }
            return endDate
        }
        
        //        func dueDateText(at index: Int) -> String {
        //            let date = node(at: index).dueDate
        //            return dateFormatter.string(from: date)
        //        }
        //
        //        func editViewModel(at index: Int) -> EditToDoItemViewController.ViewModel {
        //            let toDo = self.node(at: index)
        //            let editViewModel = EditToDoItemViewController.ViewModel(node: node)
        //            return editViewModel
        //        }
        //
        //        func addViewModel() -> EditToDoItemViewController.ViewModel {
        //            let node = Node()
        //            nodes.append(node)
        //            let addViewModel = EditToDoItemViewController.ViewModel(node: node)
        //            return addViewModel
        //        }
        //
        @objc private func removeNode(_ notification: Notification) {
            guard let userInfo = notification.userInfo,
                let assessment = userInfo[Notification.Name.deleteNodeNotification] as? Assessment,
                let index = assessments.index(of: assessment) else {
                    return
            }
            assessments.remove(at: index)
        }
        
        // MARK: Life Cycle
        init(assessments: [Assessment] = []) {
            self.assessments = assessments
            
            NotificationCenter.default.addObserver(self, selector: #selector(removeNode(_:)), name: .deleteNodeNotification, object: nil)
        }
        
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
    }
}

