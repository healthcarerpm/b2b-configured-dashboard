//
//  Questions.swift
//  Sample
//
//  Created by Thejus Jose on 29/04/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct Question: Codable{
    let Id:String
    let Sequence = 0
    let QuestionId:String? = ""
    let ParentAssessmentId:String? = ""
    let Text:String?
    let AnswerId:String
    let Value:String
    let Color:String = "red"
    let type:String
    let MeasurmentDateTime:String
    let Children: [Question]
    let MeasurementTypeId:String?
    let MeasurementValueTypeId:String?
    let MeasurementValue:String?
    
    enum CodingKeys: String, CodingKey {
        case Id
        case QuestionId
        case ParentAssessmentId
        case Text
        case AnswerId
        case Value
        case Color
        case type = "Type"
        case MeasurmentDateTime
        case Children
        case MeasurementTypeId
        case MeasurementValueTypeId
        case MeasurementValue
    }
    
    func todictionary() -> NSDictionary
    {
        
        var dict = NSMutableDictionary()
        dict["Id"] = "0336b083-8b1a-4f45-a07c-78ac7c7ef29f"
        dict["Type"] = "measurement"
        dict["Value"] = "117"
        dict["MeasurmentDateTime"] = "04/29/2019 10:20:03"
        dict["Text"] = "Weight lbs"
        dict["AnswerId"] = "B8A26400-A847-4DCD-B21F-4A846D7C12DA"
        dict["Color"] = "Green"
        dict["Sequence"] = 0
        dict["QuestionId"] = UUID().uuidString
        dict["ParentAssessmentId"] = "00000000-0000-0000-0000-000000000000"
        return dict
    }
}
