//
//  UIFunctions.swift
//  Sample
//
//  Created by Thejus Jose on 06/04/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation


class UIUtilFunctions{
    
    private static let _instance = UIUtilFunctions()
    static var Instance : UIUtilFunctions
    {
        return _instance
    }
    
    //Set UITextField Placeholder and Placeholder Color
    func setTextPlaceholderAndColor(_ placeholderName : String) -> NSAttributedString {
        let value = NSAttributedString(string: placeholderName,attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        return value
    }
    func setDisablePlaceholderAndColor(_ placeholderName : String) -> NSAttributedString {
        let value = NSAttributedString(string: placeholderName,attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        return value
    }
    func setDisablePlaceholderAndColorLightGray(_ placeholderName : String) -> NSAttributedString {
        let value = NSAttributedString(string: placeholderName,attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        return value
    }
    
    
    typealias alertControllerType = (Bool,Bool,Bool)-> Void
    
    
    //Check User Enter Valid Email or not
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"

        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    // Show Alert Message
    
    func weeksBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.weekOfYear], from: startDate, to: endDate)
        return components.weekOfYear!
    }
    
    func AlertMessage(vc: UIViewController,_ message : String,_ title:String)
    {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    }
    
    func AlertMessagePeroformAction(vc: UIViewController,_ message : String,_ title:String, handler :@escaping(UIAlertAction) -> Void)
    {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default, handler: handler)
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    }
    func TowAleternativeAlertMessagePeroformAction(vc: UIViewController,_ message : String,_ title:String, handler :@escaping(UIAlertAction) -> Void,Cancelhandler :@escaping(UIAlertAction) -> Void,_ okAction:String,_ cancelAction:String)
    {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction.init(title: okAction, style: UIAlertAction.Style.default, handler: handler)
        let cancelAction = UIAlertAction.init(title: cancelAction, style: UIAlertAction.Style.default, handler: Cancelhandler)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true, completion: nil)
    }
    
    func setEmailSecureToSendPassword(_ emailAddress:String) -> String{
        let delimiter = "@"
        var ToMakeSecureEmail = emailAddress
        let arr = ToMakeSecureEmail.components(separatedBy: delimiter)
        let textBeforeDelimiterSymbol = arr.first!
        let bunchOfTextFromMainEmailAddress = textBeforeDelimiterSymbol.prefix(2)
        let secureText = String(repeating: "*", count: textBeforeDelimiterSymbol.count-2)
        ToMakeSecureEmail =  ToMakeSecureEmail.replacingOccurrences(of: textBeforeDelimiterSymbol, with: secureText)
        return "\(bunchOfTextFromMainEmailAddress)\(ToMakeSecureEmail)"
    }
    
    func AlertForCalculatorTypeSelection(_ vc:UIViewController,_ pcHandler:@escaping(UIAlertAction) -> Void,_ ocHandler: @escaping(UIAlertAction) -> Void,_ cancelHandler:@escaping(UIAlertAction) -> Void) {
        let alert = UIAlertController(title: "Calculator", message: "Please Select Calculator", preferredStyle: .alert)
        let pregenencyCalculator = UIAlertAction(title: "Pregnancy Calculator", style: UIAlertAction.Style.default, handler: pcHandler)
        let ovulationCalendar = UIAlertAction(title: "Ovulation Calculator", style: UIAlertAction.Style.default, handler: ocHandler)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: cancelHandler)
        alert.addAction(pregenencyCalculator)
        alert.addAction(ovulationCalendar)
        alert.addAction(cancel)
        vc.present(alert, animated: true, completion: nil)
    }

}
