//
//  ActivityIndicatorExtension.swift
//  Sample
//
//  Created by Isham on 09/02/2019.
//  Copyright © 2019 Isham. All rights reserved.
//


import Foundation
import UIKit

var vSpinner : UIView?
extension UIViewController {
    
    func showSpinner(onView : UIView) {
//        UIVisualEffectView *blurredBackground = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
//        blurredBackground.frame = self.frame;
//        [self addSubview:blurredBackground];
        
        
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        
        visualEffectView.frame = onView.bounds
        
//        let spinnerView = UIView.init(frame: onView.bounds)
//        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = visualEffectView.center
        
        DispatchQueue.main.async {
            visualEffectView.contentView.addSubview(ai)
            onView.addSubview(visualEffectView)
        }
        
        vSpinner = visualEffectView
    }
    
    func removeSpinner(completion: @escaping ()-> ()) {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
            completion()
        }
    }
}
