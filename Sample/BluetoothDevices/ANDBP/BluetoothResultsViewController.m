//
//  BluetoothResultsViewController.m
//
//  Created by Joshua Ryan on 5/26/16.
//  Copyright © 2016 OdysseyInc, LLC. All rights reserved.
//

#import "BluetoothResultsViewController.h"
//#import "Constants.h"



@interface BluetoothResultsViewController ()

@property(nonatomic, assign) IBOutlet UILabel *valueOneLabel;
@property(nonatomic, assign) IBOutlet UILabel *valueTwoLabel;
@property(nonatomic, assign) IBOutlet UILabel *valueThreeLabel;

@property(nonatomic, assign) IBOutlet UILabel *titleOneLabel;
@property(nonatomic, assign) IBOutlet UILabel *titleTwoLabel;
@property(nonatomic, assign) IBOutlet UILabel *titleThreeLabel;

@property(nonatomic, strong) IBOutlet NSString *valueOne;
@property(nonatomic, strong) IBOutlet NSString *valueTwo;
@property(nonatomic, strong) IBOutlet NSString *valueThree;

@property(nonatomic, strong) IBOutlet NSString *titleOne;
@property(nonatomic, strong) IBOutlet NSString *titleTwo;
@property(nonatomic, strong) IBOutlet NSString *titleThree;

@property(nonatomic, copy) NSDictionary* readingInfo;
@property (nonatomic,nonnull,strong) NSMutableDictionary *measurementData;

@end

@implementation BluetoothResultsViewController

- (void) applyMeasurement:(NSDictionary *)measurement
{
    self.readingInfo = measurement;
    self.titleOne = @"Systolic";
    self.titleTwo = @"Diastolic";
    self.titleThree = @"Pulse";
    self.valueOne = [NSString stringWithFormat:@"%@", [self.readingInfo valueForKey:@"systolic"]];
    self.valueTwo = [NSString stringWithFormat:@"%@", [self.readingInfo valueForKey:@"diastolic"]];
    self.valueThree = [NSString stringWithFormat:@"%@", [self.readingInfo valueForKey:@"pulse"]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.valueOneLabel.text = self.valueOne;
    self.titleOneLabel.text = self.titleOne;
    
    if (self.valueTwo) {
        self.valueTwoLabel.text = self.valueTwo;
        self.titleTwoLabel.text = self.titleTwo;
    } else {
        self.valueTwoLabel.hidden = YES;
        self.titleTwoLabel.hidden = YES;
    }
    
    if (self.valueThree) {
        self.valueThreeLabel.text = self.valueThree;
        self.titleTwoLabel.text = self.titleTwo;
    } else {
        self.valueThreeLabel.hidden = YES;
        self.titleThreeLabel.hidden = YES;
    }
}

- (IBAction) onDone
{
    [self finalizeMeasurement:_readingInfo];
    
    if (self.callingController) {
        [self.navigationController popToViewController:self.callingController animated:YES];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction) onRetake
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) finalizeMeasurement:(NSDictionary *)infoDictionary {
    
    [self.delegate bluetoothResultsViewControllerDidFinishWithMeasurement:@"BP" valuedict:_readingInfo];
}

@end




