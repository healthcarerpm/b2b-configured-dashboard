//
//  MeasureRecordsViewController.swift
//  Sample
//
//  Created by Isham on 26/01/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit
import CoreData

protocol MeasureForAssessmentProtocol {
    func setResultOfMeasurement(valueSent: MeasurementData)
}

class MeasureRecordsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {

    var assessmentDelegate:MeasureForAssessmentProtocol?
    @IBOutlet weak var dateTimeHeaderLabel: UIButton!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var unitLabel1: UILabel!
    @IBOutlet weak var entryfieldView: UIView!
    @IBOutlet weak var entryFieldTextField: UITextField!
    
    @IBOutlet weak var title3: UILabel!
    
    @IBOutlet weak var unitlabel2: UILabel!
    @IBOutlet weak var subentryfieldView: UIView!
    @IBOutlet weak var subentryFieldTextField: UITextField!
    
    @IBOutlet weak var thirdEntryField: UITextField!
    
    @IBOutlet weak var thirdRecordView: UIView!
    @IBOutlet weak var unitLabel3: UILabel!
    let list = ["Blood Pressure","Blood Glucose","Oxygen","Temperature","Weight"]
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var viewType : Int = 0
    var measurement: MeasurementData! = MeasurementData()
    
    var measurementValueType_BG : String?
    var details : String?
    
    var keyboardHeight : CGFloat?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        entryFieldTextField.delegate = self
        subentryFieldTextField.delegate = self
        thirdEntryField.delegate = self
        self.view.addBackgroundImage()
        if(viewType == 2){
            self.title = "Oxygen"  //Barath changed dashboard name. Not easy to replace since db is bound to it -> Isham
        }else{
            self.title = list[viewType];
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd LLLL HH:mm"
        dateTimeHeaderLabel.setTitle(dateFormatter.string(from: Date()), for: .normal)
        dateTimeHeaderLabel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 20)!, NSAttributedString.Key.foregroundColor:UIColor.white], for: .normal)
        
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 20)!, NSAttributedString.Key.foregroundColor:UIColor.white], for: .normal)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 30)!, NSAttributedString.Key.foregroundColor: UIColor.white]
        
        self.navigationItem.titleView?.tintColor = UIColor.white
        
        entryFieldTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        setUpTheView()
        
        self.getMeasurementDetails()
    }
    
    
    
    func setUpTheView()
    {
        
        switch viewType {
        case 0:
            self.unitLabel1.text = "mmHg"
            self.unitlabel2.text = "mmHg"
            self.unitLabel3.text = "bpm"
            self.title1.text = "Systolic BP"
            self.title2.text = "Diastolic BP"
            self.title3.text = "Heart Rate"
            //removed 1 March 2020
//            self.photoView.isHidden = true
//            self.mealTimeBtn.isHidden = true
//            self.clickPicture.isHidden = true
            
        case 1:
            self.subentryfieldView.isHidden = true
            self.thirdRecordView.isHidden = true
            //removed 1 March 2020
//            self.photoView.isHidden = false
//            self.mealTimeBtn.isHidden = false
//            self.clickPicture.isHidden = false
            
            self.unitLabel1.text = "mg/dl"
            self.title1.isHidden = true
            
        case 2:
            self.unitLabel1.text = "bpm"
            self.unitlabel2.text = "%"
            
            self.title1.text = "Heart Rate"
            self.title2.text = "SPO2"
            
            self.thirdRecordView.isHidden = true
            //removed 1 March 2020
//            self.photoView.isHidden = true
//            self.mealTimeBtn.isHidden = true
//            self.clickPicture.isHidden = true

        case 3:
            self.unitLabel1.text = "F"

            self.title1.isHidden = true

            self.subentryfieldView.isHidden = true
            self.thirdRecordView.isHidden = true
            //removed 1 March 2020
//            self.photoView.isHidden = true
//            self.mealTimeBtn.isHidden = true
//            self.clickPicture.isHidden = true
            
        case 4:
            self.unitLabel1.text = "lbs"

            self.title1.isHidden = true

            self.subentryfieldView.isHidden = true
            self.thirdRecordView.isHidden = true
            //removed 1 March 2020
//            self.photoView.isHidden = true
//            self.mealTimeBtn.isHidden = true
//            self.clickPicture.isHidden = true
        default:
            print("")
        }
        
    }
    //march 1 2020
//    @objc func keyboardWillShow(notification: NSNotification) {
//        keyboardHeight = keyBoardHeight()
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0 {
//                self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.3//keyboardSize.height/2
//            }
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        if self.view.frame.origin.y != 0 {
//            self.view.frame.origin.y = 0
//        }
//    }
    
    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 150
    }
    

    @IBAction func onSubmitClicked(_ sender: UIButton) {
               
        let measurmentidentifier = MeasurementIdentifier.sharedInteractor
                
                   switch viewType {
                        
                    case 0:
                        guard let firstText = self.entryFieldTextField.text, firstText.isEmpty == false else {
                                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                                        return
                        }
                        guard let secondText = self.subentryFieldTextField.text, secondText.isEmpty == false else {
                            self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                            return
                        }
                        guard let thirdText = self.thirdEntryField.text, thirdText.isEmpty == false else {
                            self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                            return
                        }
                        
                        if measurmentidentifier.systolicEnd != nil && measurmentidentifier.systolicStart != nil
                        {
                            if ((Int(firstText)! < measurmentidentifier.systolicStart! ) || (Int(firstText)! > measurmentidentifier.systolicEnd! )){
                                
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.systolicStart!) to \(measurmentidentifier.systolicEnd!) mmHg")
                                return
                            }
                        }

                        if measurmentidentifier.heartrateEnd != nil
                            
                        {
                            if thirdText.isEmpty == false{
                                if ((Int(thirdText)! > measurmentidentifier.heartrateEnd!) || (Int(thirdText)! < measurmentidentifier.heartRateStart!)){
                                    
                                    self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.heartRateStart!) to \(measurmentidentifier.heartrateEnd!)")
                                    return
                                    
                                }
                            }
                            else{
                                 self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                            }
                            
                        }
                        break
                        
                    case 1:
                        //Blood Glucose
                        
                        guard let text = self.entryFieldTextField.text, text.isEmpty == false else {
                            self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input the glucose value")
                            
                            return
                        }
                        
                        if measurmentidentifier.bgEnd != nil && measurmentidentifier.bgStart != nil
                        {
                            if ((Double(text)! < measurmentidentifier.bgStart!) || (Double(text)! > measurmentidentifier.bgEnd!)){
                                
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.bgStart!) to \(measurmentidentifier.bgEnd!)")
                                return
                            }
                        }
                        
                        break
                        
                    case 2:
                        //Pulse
                        guard let secondText = self.subentryFieldTextField.text, secondText.isEmpty == false else{
                            self.showValoidationAlert(title: "Warning" , message: "Please input oxygen level")
                            return
                        }
                        if ((Int(secondText)! > 100 )){
                            self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid oxygen percentage")
                            return
                        }
                        
                        if measurmentidentifier.Spo2start != nil
                        {
                            if ((Int(secondText)! < measurmentidentifier.Spo2start! )){
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid oxygen percentage. Allowed minimum value is \(measurmentidentifier.Spo2start!)")
                                return
                            }
                        }
                        
                        if measurmentidentifier.heartrateEnd != nil
                        {
                            guard let firstText = self.entryFieldTextField.text, firstText.isEmpty == false else {
                                self.showValoidationAlert(title: "Warning", message: "Please input the valid heart rate")
                                return
                            }
                            
                            if ((Int(firstText)! > measurmentidentifier.heartrateEnd! )){
                                
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed maximum value is \(measurmentidentifier.heartrateEnd!)")
                                return
                                
                            }
                        }
                        break
                        
                    case 3:
                        //Temperature
                        guard let text = self.entryFieldTextField.text, text.isEmpty == false else {
                            self.showValoidationAlert(title: "Warning", message: "Please input a valid temperature")
                            return
                        }
                        
                        if measurmentidentifier.temperatueEnd != nil && measurmentidentifier.temperatureStart != nil
                        {
                            if ((Double(text)! < measurmentidentifier.temperatureStart! ) || (Double(text)! > measurmentidentifier.temperatueEnd! )){
                                
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.temperatureStart!) to \(measurmentidentifier.temperatueEnd!) ")
                                
                                return
                                
                            }
                        }
                        break
                        
                    case 4:
                        //Weight
                        guard let text = self.entryFieldTextField.text, text.isEmpty == false else {
                            self.showValoidationAlert(title: "Warning", message: "Please input a valid weight")
                            return
                        }
                        
                        if measurmentidentifier.weightEnd != nil && measurmentidentifier.weightstart != nil
                        {
                            
                            if ((Double(text)! < measurmentidentifier.weightstart! ) || (Double(text)! > measurmentidentifier.weightEnd! )){
                                
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.weightstart!) to \(measurmentidentifier.weightEnd!) ")
                                
                                return
                                
                            }
                        }
                        break
                        
                    default:
                        
                        break
                        
                    }
        measurement.self.measurementDateTime = getCurrentDateTimeString()
                    
                    if(viewType != 0){
                        
                        if(viewType == 2){
                            
                            measurement.measurementValueType = "1"
                            
                            measurement.value = String(entryFieldTextField.text!) + "/" + String(subentryFieldTextField.text!)
                            
                        }
                        else
                        {
                            measurement.value = entryFieldTextField.text!
                            
                            if measurement.measurementName == "BloodGlucose" {
                                measurement.measurementValueType = "11"
                                measurement.details = "Before Dinner"
                            }
                            else{
                                measurement.measurementValueType = "1"
                            }
                            
                        }
                    }else{
                        
                        measurement.measurementValueType = "2/3"
                        
                        measurement.value =  entryFieldTextField.text! + "/" + subentryFieldTextField.text! // + "/" + String(Int(heartRateSlider.value))
                        
                    }
                    if viewType != 0{
                        
                        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
                        
                    }
                    
                    if viewType == 2{
                        let linkedId =  UUID().uuidString
                        measurement.linkedId = linkedId
                        
                        measurement.value = String(entryFieldTextField.text!)
                        NetworkManager().uploadDataPulse(measurementData: measurement ){ (response) -> () in }
                        measurement.measurementValueType = "1"
                        measurement.value = String(subentryFieldTextField.text!)
                        measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.spo2ID
                        NetworkManager().uploadDataSPO2(measurementData: measurement ){ (response) -> () in }
                        self.navigationController?.popViewController(animated: true)
                        measurement.value = "Heart Rate: " + String(entryFieldTextField.text!) + " bpm " + "SPO2: " + String(subentryFieldTextField.text!) + "%"
                        
                        assessmentDelegate?.setResultOfMeasurement(valueSent: measurement)
                        
                    }
                        
                    else if viewType == 0{
                        let linkedId =  UUID().uuidString
                        measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.systolicID!
                        measurement.linkedId = linkedId
                        
                        if NetworkManager().networkReachability!.isReachable{
                            // Internet available
                            
                            NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
                                
                                self.measurement.value = self.thirdEntryField.text!
                                self.measurement.measurementValueType = "1"
                                self.measurement.linkedId = linkedId
                                self.measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.pulseID
                                
                                NetworkManager().uploadDataPulse(measurementData: self.measurement, completion: {(response) -> () in
                                    
                                    self.measurement.value =  self.entryFieldTextField.text! + "/" + self.subentryFieldTextField.text!  + "/" + self.thirdEntryField.text!
                                    self.measurement.isUploadedToServer = true
                                    
                                    CoreDataUtils().saveMeasurement(tablename: self.measurement.measurementName!, measurement: self.measurement)
                                    
                                    self.measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.systolicID!
                                     self.measurement.measurementValueType = "2/3"
                                    self.measurement.value = "Blood Pressure: " + self.entryFieldTextField.text! + "/" + self.subentryFieldTextField.text! + " mmHg " + " Heart Rate: " + self.thirdEntryField.text!
                                    
                                    self.assessmentDelegate?.setResultOfMeasurement(valueSent: self.measurement)
                                    
                                    self.navigationController?.popViewController(animated: true)
                                    
                                } )
                            }
                            
                        }
                        else{
                            // No Internet
                            self.measurement.value = self.thirdEntryField.text!
                            self.measurement.measurementValueType = "1"
                            self.measurement.linkedId = linkedId
                            self.measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.pulseID
                            self.measurement.isUploadedToServer = false
                            self.measurement.value =  self.entryFieldTextField.text! + "/" + self.subentryFieldTextField.text!  + "/" + self.thirdEntryField.text!
                            
                            CoreDataUtils().saveMeasurement(tablename: self.measurement.measurementName!, measurement: self.measurement)
                            
                            CoreDataUtils().saveOfflineMeasurement(tablename: "OfflineDataTable", offlineTableName: self.measurement.measurementName!, measurement: self.measurement)
                            
                            self.appDelegate.offlineSyncNeeded = true
                        }
                        
                    }
                        
                    else{
                        
                        let linkedId =  UUID().uuidString
                        measurement.linkedId = linkedId
                        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in }
                        if viewType == 1{
                            self.measurement.value = "Blood Glucose :" + self.entryFieldTextField.text! + "\(measurement.measurementUnit!)"
                        }
                        if viewType == 3{
                            self.measurement.value = "Temperature :" + self.entryFieldTextField.text! + "\(measurement.measurementUnit!)"

                        }
                        if viewType == 4{
                            self.measurement.value = "Weight :" + self.entryFieldTextField.text! + "\(measurement.measurementUnit!)"

                        }
                        assessmentDelegate?.setResultOfMeasurement(valueSent: measurement)
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
    
    func onSaveClicked() {
        let measurmentidentifier = MeasurementIdentifier.sharedInteractor
        
        if (viewType == 1) && (measurementValueType_BG == nil) {
            showValoidationAlert(title: "Alert", message: "Please select meal")
        }
            
            
        else {
            switch viewType {
                
            case 0:
                guard let firstText = self.entryFieldTextField.text, firstText.isEmpty == false else {
                                self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                                return
                }
                guard let secondText = self.subentryFieldTextField.text, secondText.isEmpty == false else {
                    self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                    return
                }
                guard let thirdText = self.thirdEntryField.text, thirdText.isEmpty == false else {
                    self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                    return
                }
                
                if measurmentidentifier.systolicEnd != nil && measurmentidentifier.systolicStart != nil
                {
                    if ((Int(firstText)! < measurmentidentifier.systolicStart! ) || (Int(firstText)! > measurmentidentifier.systolicEnd! )){
                        
                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.systolicStart!) to \(measurmentidentifier.systolicEnd!) mmHg")
                        return
                    }
                }

                if measurmentidentifier.heartrateEnd != nil
                    
                {
                    if thirdText.isEmpty == false{
                        if ((Int(thirdText)! > measurmentidentifier.heartrateEnd!) || (Int(thirdText)! < measurmentidentifier.heartRateStart!)){
                            
                            self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.heartRateStart!) to \(measurmentidentifier.heartrateEnd!)")
                            return
                            
                        }
                    }
                    else{
                         self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input all the fields")
                    }
                    
                }
                break
                
            case 1:
                //Blood Glucose
                
                guard let text = self.entryFieldTextField.text, text.isEmpty == false else {
                    self.showValoidationAlert(title: self.list[self.viewType] , message: "Please input the glucose value")
                    
                    return
                }
                
                if measurmentidentifier.bgEnd != nil && measurmentidentifier.bgStart != nil
                {
                    if ((Double(text)! < measurmentidentifier.bgStart!) || (Double(text)! > measurmentidentifier.bgEnd!)){
                        
                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.bgStart!) to \(measurmentidentifier.bgEnd!)")
                        return
                    }
                }
                
                break
                
            case 2:
                //Pulse
                guard let secondText = self.subentryFieldTextField.text, secondText.isEmpty == false else{
                    self.showValoidationAlert(title: "Warning" , message: "Please input oxygen level")
                    return
                }
                if ((Int(secondText)! > 100 )){
                    self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid oxygen percentage")
                    return
                }
                
                if measurmentidentifier.Spo2start != nil
                {
                    if ((Int(secondText)! < measurmentidentifier.Spo2start! )){
                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid oxygen percentage. Allowed minimum value is \(measurmentidentifier.Spo2start!)")
                        return
                    }
                }
                
                if measurmentidentifier.heartrateEnd != nil
                {
                    guard let firstText = self.entryFieldTextField.text, firstText.isEmpty == false else {
                        self.showValoidationAlert(title: "Warning", message: "Please input the valid heart rate")
                        return
                    }
                    
                    if ((Int(firstText)! > measurmentidentifier.heartrateEnd! )){
                        
                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed maximum value is \(measurmentidentifier.heartrateEnd!)")
                        return
                        
                    }
                }
                break
                
            case 3:
                //Temperature
                guard let text = self.entryFieldTextField.text, text.isEmpty == false else {
                    self.showValoidationAlert(title: "Warning", message: "Please input a valid temperature")
                    return
                }
                
                if measurmentidentifier.temperatueEnd != nil && measurmentidentifier.temperatureStart != nil
                {
                    if ((Double(text)! < measurmentidentifier.temperatureStart! ) || (Double(text)! > measurmentidentifier.temperatueEnd! )){
                        
                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.temperatureStart!) to \(measurmentidentifier.temperatueEnd!) ")
                        
                        return
                        
                    }
                }
                break
                
            case 4:
                //Weight
                guard let text = self.entryFieldTextField.text, text.isEmpty == false else {
                    self.showValoidationAlert(title: "Warning", message: "Please input a valid weight")
                    return
                }
                
                if measurmentidentifier.weightEnd != nil && measurmentidentifier.weightstart != nil
                {
                    
                    if ((Double(text)! < measurmentidentifier.weightstart! ) || (Double(text)! > measurmentidentifier.weightEnd! )){
                        
                        self.showValoidationAlert(title: self.list[self.viewType] , message: "Please enter valid mesasurement. Allowed range is \(measurmentidentifier.weightstart!) to \(measurmentidentifier.weightEnd!) ")
                        
                        return
                        
                    }
                }
                break
                
            default:
                
                break
                
            }
            
            measurement.measurementDateTime = getCurrentDateTimeString()
            
            if(viewType != 0){
                
                if(viewType == 2){
                    
                    measurement.measurementValueType = "1"
                    
                    measurement.value = String(entryFieldTextField.text!) + "/" + String(subentryFieldTextField.text!)
                    
                }
                else
                {
                    measurement.value = entryFieldTextField.text!
                    
                    if measurement.measurementName == "BloodGlucose" {
                        measurement.measurementValueType = "11"//measurementValueType_BG
                        measurement.details = details
                    }
                    else{
                        measurement.measurementValueType = "1"
                    }
                    
                }
            }else{
                
                measurement.measurementValueType = "2/3"
                
                measurement.value =  entryFieldTextField.text! + "/" + subentryFieldTextField.text! // + "/" + String(Int(heartRateSlider.value))
                
            }
            if viewType != 0{
                
                CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
                
            }
            
            if viewType == 2{
                let linkedId =  UUID().uuidString
                measurement.linkedId = linkedId
                
                measurement.value = String(entryFieldTextField.text!)
                NetworkManager().uploadDataPulse(measurementData: measurement ){ (response) -> () in }
                measurement.measurementValueType = "1"
                measurement.value = String(subentryFieldTextField.text!)
                measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.spo2ID
                NetworkManager().uploadDataSPO2(measurementData: measurement ){ (response) -> () in }
                self.navigationController?.popViewController(animated: true)
                measurement.value = "Heart Rate: " + String(entryFieldTextField.text!) + " bpm " + "SPO2: " + String(subentryFieldTextField.text!) + "%"
                
                assessmentDelegate?.setResultOfMeasurement(valueSent: measurement)
                
            }
                
            else if viewType == 0{
                let linkedId =  UUID().uuidString
                measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.systolicID!
                measurement.linkedId = linkedId
                
                if NetworkManager().networkReachability!.isReachable{
                    // Internet available
                    
                    NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
                        
                        self.measurement.value = self.thirdEntryField.text!
                        self.measurement.measurementValueType = "1"
                        self.measurement.linkedId = linkedId
                        self.measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.pulseID
                        
                        NetworkManager().uploadDataPulse(measurementData: self.measurement, completion: {(response) -> () in
                            
                            self.measurement.value =  self.entryFieldTextField.text! + "/" + self.subentryFieldTextField.text!  + "/" + self.thirdEntryField.text!
                            self.measurement.isUploadedToServer = true
                            
                            CoreDataUtils().saveMeasurement(tablename: self.measurement.measurementName!, measurement: self.measurement)
                            
                            // CoreDataUtils().saveOfflineMeasurement(tablename: "OfflineDataTable", offlineTableName: self.measurement.measurementName!, measurement: self.measurement)
                            
                            self.measurement.value = "Blood Pressure: " + self.entryFieldTextField.text! + "/" + self.subentryFieldTextField.text! + " mmHg " + " Heart Rate: " + self.thirdEntryField.text!
                            
                            self.assessmentDelegate?.setResultOfMeasurement(valueSent: self.measurement)
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        } )
                    }
                    
                }
                else{
                    // No Internet
                    self.measurement.value = self.thirdEntryField.text!
                    self.measurement.measurementValueType = "1"
                    self.measurement.linkedId = linkedId
                    self.measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.pulseID
                    self.measurement.isUploadedToServer = false
                    self.measurement.value =  self.entryFieldTextField.text! + "/" + self.subentryFieldTextField.text!  + "/" + self.thirdEntryField.text!
                    
                    CoreDataUtils().saveMeasurement(tablename: self.measurement.measurementName!, measurement: self.measurement)
                    
                    CoreDataUtils().saveOfflineMeasurement(tablename: "OfflineDataTable", offlineTableName: self.measurement.measurementName!, measurement: self.measurement)
                    
                    self.appDelegate.offlineSyncNeeded = true
                }
                
            }
                
            else{
                
                let linkedId =  UUID().uuidString
                measurement.linkedId = linkedId
                NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in }
                if viewType == 1{
                    self.measurement.value = "Blood Glucose :" + self.entryFieldTextField.text! + "\(measurement.measurementUnit!)"
                }
                if viewType == 3{
                    self.measurement.value = "Temperature :" + self.entryFieldTextField.text! + "\(measurement.measurementUnit!)"

                }
                if viewType == 4{
                    self.measurement.value = "Weight :" + self.entryFieldTextField.text! + "\(measurement.measurementUnit!)"

                }
                assessmentDelegate?.setResultOfMeasurement(valueSent: measurement)
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    
    
    
//    @IBAction func clickPictureBtn(_ sender: Any) {
//
//        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//            self.openCamera()
//        }))
//
//        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//            self.openGallery()
//        }))
//
//        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//
//        if let popoverController = alert.popoverPresentationController {
//            popoverController.sourceView = clickPicture
//            if #available(iOS 12.0, *) {
//                popoverController.sourceRect = clickPicture.bounds
//                popoverController.permittedArrowDirections = [.up]
//            } else {
//                popoverController.sourceRect = self.view.frame
//            }
//        }
//
//
//        self.present(alert, animated: true, completion: nil)
//
//    }
    
    
//    @IBAction func mealTimeBtnClick(_ sender: Any) {
//        let actionsheet = UIAlertController()
//
//        actionsheet.view.backgroundColor = UIColor.white
//
//
//        let breakfastbefore = UIAlertAction(title: "Before Breakfast", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("Before Breakfast", for: .normal)
//            self.measurementValueType_BG = "10"
//            self.details = "Before Breakfast"
//        })
//        actionsheet.addAction(breakfastbefore)
//        breakfastbefore.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//        let afterbreakfast = UIAlertAction(title: "After Breakfast", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("After Breakfast", for: .normal)
//            self.measurementValueType_BG = "11"
//            self.details = "After Breakfast"
//        })
//        actionsheet.addAction(afterbreakfast)
//        afterbreakfast.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//        let beforemeal = UIAlertAction(title: "Before Meal", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("Before Meal", for: .normal)
//            self.measurementValueType_BG = "10"
//            self.details = "Before Meal"
//        })
//        actionsheet.addAction(beforemeal)
//        beforemeal.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//
//        let aftermeal = UIAlertAction(title: "After Meal", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("After Meal", for: .normal)
//            self.measurementValueType_BG = "11"
//            self.details = "After Meal"
//        })
//        actionsheet.addAction(aftermeal)
//        aftermeal.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//        let beforedinner = UIAlertAction(title: "Before Dinner", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("Before Dinner", for: .normal)
//            self.measurementValueType_BG = "10"
//            self.details = "Before Dinner"
//        })
//        actionsheet.addAction(beforedinner)
//        beforedinner.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//        let afterdinner = UIAlertAction(title: "After Dinner", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("After Dinner", for: .normal)
//            self.measurementValueType_BG = "11"
//            self.details = "After Dinner"
//        })
//        actionsheet.addAction(afterdinner)
//        afterdinner.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//
//
//        let other = UIAlertAction(title: "After Midnight Snack", style: .default, handler: {action -> Void in
//            self.mealTimeBtn.setTitle("After Midnight Snack", for: .normal)
//            self.measurementValueType_BG = "11"
//            self.details = "After Midnight Snack"
//        })
//
//        actionsheet.addAction(other)
//        other.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
//
//        if let popoverController = actionsheet.popoverPresentationController {
//            popoverController.sourceView = mealTimeBtn
//            if #available(iOS 12.0, *) {
//                popoverController.sourceRect = mealTimeBtn.bounds
//                popoverController.permittedArrowDirections = [.up]
//            } else {
//                popoverController.sourceRect = self.view.frame
//            }
//        }
//        self.present(actionsheet, animated: true, completion: nil)
//    }
    
    func getMeasurementDetailsSPO2() -> MeasurementData {
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        let tableName = "SPO2"
        measurement.measurementName = self.list[viewType]
        request.predicate = NSPredicate(format: "name = %@",  "SPO2")
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = true
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        } catch {
            print("Failed")
        }
        
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeString()
        return measurement
        
    }
// removed 1 March 2020
//    func setupChooseBtn(){
//        photoView.isHidden = false
//        mealTimeBtn.isHidden = false
//        clickPicture.isHidden = false
//        clickPicture.layer.cornerRadius = 20
//        clickPicture.layer.borderWidth = 1
//        clickPicture.layer.borderColor = UIColor.blue.cgColor
//    }
    
    func getMeasurementDetails() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        var tableName = ""
        if(self.list[viewType] == "Blood Pressure"){
            tableName = "BloodPressure"
        }else if (self.list[viewType] == "Blood Glucose"){
            tableName = "BloodGlucose"
            //removed 1 March 2020
        }else if (self.list[viewType] == "Oxygen"){
            tableName = "Pulse"
        }else if (self.list[viewType] == "Weight"){
            tableName = "Weight"
        }else if (self.list[viewType] == "Temperature"){
            tableName = "Temperature"
        }
        measurement.measurementName = tableName;
        if tableName == "Pulse"{
            let table = "HeartRate"
            request.predicate = NSPredicate(format: "name = %@",  table)
            request.returnsObjectsAsFaults = false
        }else{
            request.predicate = NSPredicate(format: "name = %@",  tableName)
            request.returnsObjectsAsFaults = false
        }
        measurement.isManual = true
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        } catch {
            print("Failed")
        }
        
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        
        if measurement.measurementUnit == nil{
        }else
        {
            unitLabel1.text = measurement.measurementUnit!
            if(viewType == 2){
                unitlabel2.text = "%"
            }else if(viewType == 0){
                unitlabel2.text = measurement.measurementUnit
            }
        }
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch viewType {
        case 1:
            break
        default:
            break
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
//        photoView.image = selectedImage
//        photoView.contentMode = .scaleAspectFit
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func getCurrentDateTimeString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateTimeFormatter.timeZone = TimeZone.current
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        return str
    }
    
    func showValoidationAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }
    
    
    
    override func didMove(toParent parent: UIViewController?) {
        if (parent == nil) {
            print("Back Button Pressed!")
            
        }
        super.didMove(toParent: parent)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //["Blood pressure","Blood glucose","Pulse","Temperature","Weight"]
        if textField == entryFieldTextField || textField == subentryFieldTextField || textField == thirdEntryField {
            
            if viewType == 3 || viewType == 2{
                if string == "0" && range.location == 0 {
                    return false
                }
            }
            
            if viewType == 0 || viewType == 2{
                if string.contains("."){
                    return false
                }
            }
            
            if viewType == 0 || viewType == 2 {
                if range.location > 2{
                    return false
                }
                
            }
            
            if viewType == 1 || viewType == 4 || viewType == 3{
                if range.location > 4{
                    return false
                }
            }
            
            let allowedCharacters = CharacterSet(charactersIn:".0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    
    
}


