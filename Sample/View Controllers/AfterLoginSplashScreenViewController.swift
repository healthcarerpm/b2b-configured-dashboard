//
//  AfterLoginSplashScreenViewController.swift
//  Sample
//
//  Created by Thejus Jose on 19/03/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation


class AfterLoginSplashScreenViewController : ViewController {
    
    var splash:UIImage? = nil
    
    let BASE_URL = "https://healthcarerpmapi.azurewebsites.net/"
    // let BASE_URL = "https://brainmeshintegrumapi.azurewebsites.net/"
    
    
    @IBOutlet weak var splashScreenLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appendingPathComponent = "photo/temp/splashLogo/"
        var currentPath = ""
        var  splashScreenAppendPath:String? = nil
        //??? what if customer id s not available for patient
        guard let customerId = UserDefaults.standard.value(forKey: Constants.USER_DEF_CUSTOMER_ID) as? String
            else { return }
        
        if let localPath = UserDefaults.standard.value(forKey: Constants.USER_DEF_SPLASH_LOGO_PATH)  as? String{
            currentPath = localPath
            //drop .png or .jpg extensions
            currentPath = currentPath.deletingSuffux(".png")
            currentPath = currentPath.deletingSuffux(".gif")
            currentPath = currentPath.deletingSuffux(".jpg")
            currentPath = currentPath.deletingSuffux(".jpeg")
            
        }
        let tempPath = appendingPathComponent + customerId
        
        guard tempPath == currentPath
            else {
//                print("##### Image not found +\(UserDefaults.standard.value(forKey: Constants.USER_DEF_SPLASH_LOGO_PATH) as? String)")
                // save image (way 1)
                self.getAndSaveCustomerLogo(customerId: customerId,appendingPath: appendingPathComponent) { (imageURLPath) -> Void in
                    
//                    print("imageURL=\(imageURLPath)")
                    if imageURLPath != nil{
                        
                        splashScreenAppendPath = imageURLPath
                        
                        let imageURL:URL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imageURLPath!)
                    }
                    
                    
                }//end of NetworkManager().getCustomerLogo
                return
                
        }
//        if(splashScreenAppendPath != nil )
//        {
//            let imgURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(splashScreenAppendPath!)
//            self.splashScreenLogo.image = UIImage(fileURLWithPath: imgURL)
//            self.splashScreenLogo.setNeedsLayout()
//            self.splashScreenLogo.setNeedsDisplay()
//        }
        
    }//enf of viewDidLoad
    
    
    /**
     
     guard   let img = UIImage(named: "img"),
     let url = img.save(at: .documentDirectory,
     pathAndImageName: path)
     else { return }
     print(url)
     
     //        // save image (way 2)
     //        let tempDirectoryUrl = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(path)
     //        guard let url2 = img2.save(at: tempDirectoryUrl) else { return }
     //        print(url2)
     
     */
    
    // get image from directory
    //        guard let img2 = UIImage(fileURLWithPath: url) else { return }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.addConstraint(TopSafeAreaConstraint())
        if MeasurementIdentifier.sharedInteractor.isEmployer == 1{
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                self.performSegue(withIdentifier: "afterSplashScreenInstructions", sender: nil)
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                self.performSegue(withIdentifier: "afterSplashScreen", sender: nil)
            }
        }
    }
    
    class TopSafeAreaConstraint: NSLayoutConstraint {
        override func awakeFromNib() {
            super.awakeFromNib()
            if #available(iOS 11.0, *) {
                let insets = UIApplication.shared.keyWindow?.safeAreaInsets ?? .zero
                self.constant = max(insets.top, 20)
            } else {
                // Pre-iOS 11.0
                self.constant = 20.0
            }
        }
    }
    
    
    /**
     Get Splash Screen logo of the customer
     */
    fileprivate func getAndSaveCustomerLogo(customerId: String,appendingPath: String, completion: @escaping (_ image:String?) -> Void){
        completion(nil)
        
        let apiToken = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        
        guard let imgURL = URL(string: BASE_URL+"api/customer/downloadmediafile"+"?customerId="+customerId+"&api_key="+apiToken) else {
            completion(nil)
            return
            
        }
        
        print("image URL==\(imgURL)");
        let session = URLSession(configuration: .default)
        
        
        let downloadPicTask = session.dataTask(with: imgURL) { (data, response, error) in
            if let e = error {
                print("Error downloading cat picture: \(e)")
                
            } else {
                if let res = response as? HTTPURLResponse {
                    UserDefaults.standard.set(appendingPath, forKey: Constants.USER_DEF_SPLASH_LOGO_PATH)
                    
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if res.statusCode == 200 {
                        if let imageData = data{
                            DispatchQueue.main.async {
                                let image = UIImage(data: imageData)
                                LogoResource.sharedInteractor.splashLogo = image
                                self.splashScreenLogo.image = image
                                self.splashScreenLogo.setNeedsLayout()
                                self.splashScreenLogo.setNeedsDisplay()
                                print("image downloaded and saved")
                            }
                        }
                    } else {
                        print("Couldn't get image: Image is nil")
                        DispatchQueue.main.async {
                            let npmLogo = UIImage(named: "npm_logo")
                            LogoResource.sharedInteractor.splashLogo = npmLogo
                            self.splashScreenLogo.image = npmLogo
                            self.splashScreenLogo.setNeedsLayout()
                            self.splashScreenLogo.setNeedsDisplay()
                            completion(nil)
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        let npmLogo = UIImage(named: "npm_logo")
                        LogoResource.sharedInteractor.splashLogo = npmLogo
                        self.splashScreenLogo.image = npmLogo
                        self.splashScreenLogo.setNeedsLayout()
                        self.splashScreenLogo.setNeedsDisplay()
                        completion(nil)
                    }
                    print("Couldn't get response code for some reason")
                    completion(nil)
                }
            }
        }
        downloadPicTask.resume()
    }
}

// save
extension UIImage {
    
    func save(at directory: FileManager.SearchPathDirectory,
              pathAndImageName: String,
              createSubdirectoriesIfNeed: Bool = true,
              compressionQuality: CGFloat = 1.0)  -> URL? {
        do {
            let documentsDirectory = try FileManager.default.url(for: directory, in: .userDomainMask,
                                                                 appropriateFor: nil,
                                                                 create: false)
            return save(at: documentsDirectory.appendingPathComponent(pathAndImageName),
                        createSubdirectoriesIfNeed: createSubdirectoriesIfNeed,
                        compressionQuality: compressionQuality)
        } catch {
            print("-- Error: \(error)")
            return nil
        }
    }
    
    func save(at url: URL,
              createSubdirectoriesIfNeed: Bool = true,
              compressionQuality: CGFloat = 1.0)  -> URL? {
        do {
            if createSubdirectoriesIfNeed {
                try FileManager.default.createDirectory(at: url.deletingLastPathComponent(),
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            }
            guard let data = jpegData(compressionQuality: compressionQuality) else { return nil }
            try data.write(to: url)
            return url
        } catch {
            print("-- Error: \(error)")
            return nil
        }
    }
}

// load from path

extension UIImage {
    convenience init?(fileURLWithPath url: URL, scale: CGFloat = 1.0) {
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data, scale: scale)
        } catch {
            print("-- Error: \(error)")
            return nil
        }
    }
}

extension String {
    func deletingSuffux(_ suffix: String) -> String {
        guard self.hasSuffix(suffix) else { return self }
        return String(self.dropLast(suffix.count))
    }
}






//            let imgURL = URL(string: self.viewModel.text!)!
//            //                print("image URL=="imgURL);
//            let session = URLSession(configuration: .default)
//
//            let downloadPicTask = session.dataTask(with: imgURL) { (data, response, error) in
//                if let e = error {
//                    print("Error downloading logo picture: \(e)")
//
//                } else {
//                    if let res = response as? HTTPURLResponse {
//                        print("Downloaded logo picture with response code \(res.statusCode)")
//                        if let imageData = data {
//                            DispatchQueue.main.async {
//                                let image = UIImage(data: imageData)
//                                self.splashScreenLogo.image = image
//                                self.splashScreenLogo.setNeedsLayout()
//                                self.splashScreenLogo.setNeedsDisplay()
//                                self.splashScreenLogo.setNeedsLayout()
//                                self.splashScreenLogo.setNeedsDisplay()
//                                print("is it displayed")
//                            }
//                        } else {
//                            print("Couldn't get image: Image is nil")
//
//                        }
//
//                    } else {
//                        print("Couldn't get response code for some reason")
//                    }
//                }
//            }
//            downloadPicTask.resume()



//  Construct the view for the cell
//        cell.view = UIImageView()

//        self.splashScreenLogo.viewRightMargin = 0.0
//        self.splashScreenLogo.viewLeftMargin = 0.0
//        self.splashScreenLogo.viewTopMargin = 0.0
//        self.splashScreenLogo.viewBottomMargin = 0.0
//
//        self.splashScreenLogo.height = { return CGFloat(200) }

//        cell.contentView.addSubview(cell.view!)











