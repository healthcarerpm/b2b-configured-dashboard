//
//  DetailsViewController.swift
//  Sample
//
//  Created by Isham on 28/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit
import CoreData

class RecordsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
}

class DetailsViewController: UIViewController, LineChartDelegate, UITableViewDataSource, UITableViewDelegate,WeightScaleMeasurementCaptureProtocol,BluetoothReadingViewControllerDelegate,NoninInputViewControllerDelegate{
  
    func didSelectYDataPoint(_ y: CGFloat, xValues: [Any]) {
        
    }
    
 
    @IBOutlet weak var chartNameLabel: UILabel!
    
    func noninInputController(_ controller: NoninInputViewController!, didCompleteWithMeasurement measurements: [AnyHashable : Any]!) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Pulse";
        
        let table = "HeartRate"
        request.predicate = NSPredicate(format: "name = %@", table)// measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        
        measurement.measurementDateTime = getCurrentDateTimeasString()
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId

        let pulse = measurements["pulse"] as! Int
        let spo2 = measurements["spo2"] as! Int
        measurement.value =  String(pulse) + "/" + String(spo2)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        
        measurement.value = String(pulse)
        
        NetworkManager().uploadDataPulse(measurementData: measurement ){ (response) -> () in
        }
        
        measurement.value = String(spo2)
        measurement.isManual = false
        measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.spo2ID
        
        NetworkManager().uploadDataSPO2(measurementData: measurement, completion: { (response) -> () in
        })
        
    }


    func weightScaleDidCaptureMeasurement(_ value:Double, dateCaptured date: Date) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Weight";
        request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeasString()
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId

        let weight = value
        measurement.value =  String(weight)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
        }
        
    }
    var viewType : Int = 0
    
    @IBOutlet weak var chartView: LineChart!
    @IBOutlet weak var recordsTableView: UITableView!
    @IBOutlet weak var useDeviceButton: UIButton!
    @IBOutlet weak var recordManuallyButton: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    
    let list = ["Blood pressure","Blood glucose","Pulse","Temperature","Weight"]
    var measurementData = [MeasurementData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(viewType == 2){
            self.title = "SPO2"  //Barath changed dashboard name. Not easy to replace since db is bound to it -> Isham
        }else{
            self.title = list[viewType];
        }
        self.chartNameLabel.text = list[viewType].uppercased()
        self.initChat()
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 30), NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        self.recordsTableView.register(Measurementcell.self, forCellReuseIdentifier: "cell")
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.title = "Isham"
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        measurementData = reloadData()
        recordsTableView.reloadData()
        self.reloadGraph()
        super.viewDidAppear(animated)
    }

    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "back_arrow"), for: UIControl.State())
        btnLeftMenu.titleLabel?.text = "<"
        btnLeftMenu.addTarget(self, action: #selector(DetailsViewController.onClcikBack), for: UIControl.Event.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 33/2, height: 27/2)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func onClcikBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func onUseDeviceButtonClicked(_ sender: Any) {
        
        if (viewType == 0){
            //BP
            let AndBPstoryboard = UIStoryboard(name: "BluetoothReading", bundle: nil)
            let nextVC = AndBPstoryboard.instantiateViewController(withIdentifier: "AnDBPStoryboard") as! BluetoothReadingViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
            
        else if (viewType == 2){
            //pulseOx
            let NoninStoryboard = UIStoryboard(name: "NoninPulseOxStoryboard" , bundle: nil)
            let nextVC = NoninStoryboard.instantiateViewController(withIdentifier: "NoninInputViewController") as! NoninInputViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
        else if(viewType == 4){
            //weight
            let AnDStoryboard = UIStoryboard(name: "andWeightScaleStoryboard" , bundle: nil)
            let nextVC = AnDStoryboard.instantiateViewController(withIdentifier: "A&DWeightScale") as! AnDWeightScaleViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
        
    }
    @IBAction func onRecordManuallyButtonClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "measureData") as! MeasureRecordsViewController
        nextViewController.viewType = self.viewType
        self.navigationController?.show(nextViewController, sender: self)
    }
    
    func initChat (){
        let data2: [CGFloat] = [0,0,0,0,0,0,]
        let xLabels: [String] = ["","","","","","","","","",""]
        
        
        chartView.animation.enabled = true
        chartView.area = true
        chartView.x.labels.visible = false
        chartView.y.labels.visible = false
        chartView.x.grid.count = 6
        chartView.y.grid.count = 6
        chartView.x.labels.values = xLabels
        chartView.addLine(data2)
        
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.delegate = self
    }
    
    /**
     * Line chart delegate method.
     */
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>) {
    }
    
    func getDateForGraph(date: String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let graphDateFormatter = DateFormatter()
        graphDateFormatter.dateFormat = "MM-dd"
        if let date = dateFormatter.date(from: date)
        {
            return graphDateFormatter.string(from: date)
        }
        else{
            return ""
        }
    }

    
    /**
     * Redraw chart on device rotation.
     */
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if let chart = chartView {
            chart.setNeedsDisplay()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if measurementData.count > 0{
           return measurementData.count + 1
        }
        else{
        return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        let cell = Measurementcell.init(style: .default, reuseIdentifier: "cell")
        
        let dotString = " • "
        
        if indexPath.row == 0 && viewType == 0{
           // let main_string = "• Systolic"
            let main_string = "Date"
            
            let range = (main_string as NSString).range(of: dotString)
          //  cell.label?.font = UIFont.init(name: "HelveticaNeue-Bold", size: 20)!
             cell.label?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            
            let attributedString = NSMutableAttributedString(string:main_string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: colors[0] , range: range)
            cell.label?.attributedText = getAttributedStringForChart(mainString: main_string, strigToBeAttributed: dotString, color: colors[0])

           // let header2 = "• Diastolic"
            let header2 = "Time"
            cell.label1?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label1?.attributedText = getAttributedStringForChart(mainString: header2, strigToBeAttributed: dotString, color: colors[1])
            

            //let header3 = "• Heart Rate"
            let header3 = "• Systolic"
            cell.label2?.text = header3
            cell.label2?.textColor = colors[0]
      
            
             cell.label2?.attributedText = getAttributedStringForChart(mainString: header3, strigToBeAttributed: dotString, color: colors[2])
            cell.label2?.font = UIFont.init(name: "Bogle-Bold", size: 18)!

           // cell.label3?.text = "Measurement Date"
            cell.label3?.text = "• Diastolic"
            cell.label3?.textColor = colors[1]
            cell.label3?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            
            cell.label4?.text = "• Heart Rate"
            cell.label4?.textColor = colors[2]
            cell.label4?.font = UIFont.init(name: "Bogle-Bold", size: 18)!

            return cell
        }
        
        if indexPath.row == 0 && viewType == 2{
          //  let header1 = " • Heart Rate"
            let header1 = "Date"
            cell.label?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label?.attributedText = getAttributedStringForChart(mainString: header1, strigToBeAttributed: dotString, color: colors[0])

           // let header2 = "• SPO2"
            let header2 = "Time"
            cell.label1?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label1?.attributedText = getAttributedStringForChart(mainString: header2, strigToBeAttributed: dotString, color: colors[1])

            
            cell.label2?.text = "• Heart Rate"
            cell.label2?.textColor = colors[0]
            cell.label2?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            
            cell.label3?.text = "• SPO2"
            cell.label3?.textColor = colors[1]
            cell.label3?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label4?.isHidden = true
            

            return cell
        }
        
        if indexPath.row == 0 && viewType == 1
        {
           // let header1 = "• Blood Glucose"
            let header1 = "Date"
            cell.label?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label?.attributedText = getAttributedStringForChart(mainString: header1, strigToBeAttributed: dotString, color: colors[0])
          
            let header2 = "Time"
            cell.label1?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label1?.attributedText = getAttributedStringForChart(mainString: header2, strigToBeAttributed: dotString, color: colors[1])


            cell.label2?.text = "•Blood Glucose"
            cell.label2?.textColor = colors[0]
            cell.label2?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label3?.isHidden = true
            cell.label4?.isHidden = true
            

            return cell
        }
        
        if indexPath.row == 0 && viewType == 3
        {
           // let header1 = "• Temperature"
            let header1 = "Date"
            cell.label?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label?.attributedText = getAttributedStringForChart(mainString: header1, strigToBeAttributed: dotString, color: colors[0])
            
            let header2 = "Time"
            cell.label1?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label1?.attributedText = getAttributedStringForChart(mainString: header2, strigToBeAttributed: dotString, color: colors[1])

            cell.label2?.text = "• Temperature"
            cell.label2?.textColor = colors[0]
            cell.label2?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label3?.isHidden = true
            cell.label4?.isHidden = true
            return cell
        }

        if indexPath.row == 0 && viewType == 4
        {
            // let header1 = "• Weight"
             let header1 = "Date"
            cell.label?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label?.attributedText = getAttributedStringForChart(mainString: header1, strigToBeAttributed: dotString, color: colors[0])
           
            let header2 = "Time"
            cell.label1?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label1?.attributedText = getAttributedStringForChart(mainString: header2, strigToBeAttributed: dotString, color: colors[1])
            
            cell.label2?.text = "• Weight"
            cell.label2?.textColor = colors[0]
            cell.label2?.font = UIFont.init(name: "Bogle-Bold", size: 18)!
            cell.label3?.isHidden = true
            cell.label4?.isHidden = true
            return cell
        }

        var measurement = measurementData[indexPath.row - 1]
        var dataValue = ""
        if measurement.measurementUnit != nil
        {
         dataValue = measurement.value! + " " + measurement.measurementUnit!
        }else{
             dataValue = measurement.value!
        }
        
        if (viewType == 0){
            var bpVals = ""
            if measurement.measurementUnit != nil{
             bpVals = measurement.value!.components(separatedBy: "/")[0] + "/" + measurement.value!.components(separatedBy: "/")[1] + " " + measurement.measurementUnit!
            }else{
                bpVals = measurement.value!.components(separatedBy: "/")[0] + "/" + measurement.value!.components(separatedBy: "/")[1]
            }
            
            let systolic = measurement.value!.components(separatedBy: "/")[0]
            let diastolic = measurement.value!.components(separatedBy: "/")[1]
            do {
                if(measurement.value!.components(separatedBy: "/").count > 2){
                dataValue = bpVals + " " + measurement.value!.components(separatedBy: "/")[2] + " BPM"
            let HR = measurement.value!.components(separatedBy: "/")[2]
                cell.label4?.text = HR + " bpm"
                }
            }catch let error as NSError {
            
            }
            
            cell.label2?.text = systolic + " mmHg"
            cell.label3?.text = diastolic + " mmHg"
            
          //  cell.label?.text = getDateascontinuesStrings(datefromfunction: measurement.measurementDateTime!)
            let onlyDate = getDate(getdate: measurement.measurementDateTime!)
            let onlyTime = getTime(getTime: measurement.measurementDateTime!)
            cell.label?.text = onlyDate
            cell.label1?.text = onlyTime
            
            return cell
        }
        
        if (viewType == 2){

            let hr = measurement.value!.components(separatedBy: "/")[0]
            if(measurement.value!.components(separatedBy: "/").count > 1){
                let spo2 = measurement.value!.components(separatedBy: "/")[1]
                cell.label3?.text = spo2 + "%"
            }
            
            cell.label2?.text = hr + " bpm"
            
          //  cell.label2?.text = getDateascontinuesStrings(datefromfunction: measurement.measurementDateTime!)
            let onlyDate = getDate(getdate: measurement.measurementDateTime!)
            let onlyTime = getTime(getTime: measurement.measurementDateTime!)
            cell.label?.text = onlyDate
            cell.label1?.text = onlyTime
            cell.label4?.isHidden = true
            return cell
        }
        
        let value = measurement.value!
        
        let onlyDate = getDate(getdate: measurement.measurementDateTime!)
        let onlyTime = getTime(getTime: measurement.measurementDateTime!)
        cell.label?.text = onlyDate
        cell.label1?.text = onlyTime
        
        cell.label2?.text = value + " " +  measurement.measurementUnit!
       // cell.label1?.text = getDateascontinuesStrings(datefromfunction: measurement.measurementDateTime!)
      //  cell.label1?.frame = CGRect(x: 200, y: 0, width: 500, height: 50)
        
        cell.label3?.isHidden = true
        cell.label4?.isHidden = true
        return cell

     
        
    }
    
    
    func reloadData() ->[MeasurementData]{
        let coreDataUtils = CoreDataUtils()
        var tableName = ""
        if(self.list[viewType] == "Blood pressure"){
            tableName = "BloodPressure"
        }else if (self.list[viewType] == "Blood glucose"){
            tableName = "BloodGlucose"
        }else{
            tableName = self.list[viewType]
        }
        return coreDataUtils.getMeasurement(tablename: tableName)
    }
    
    func reloadGraph() {
        var xLabelData : [String] = []
        let measurementDataArray = measurementData.reversed()
        if viewType == 0 {
            var systolicData : [CGFloat] = []
            var diastolicData : [CGFloat] = []
            var heartrateData : [CGFloat] = []
            
            for measurement in measurementDataArray {
                let data: [String] =  (measurement.value?.components(separatedBy: "/"))!
                systolicData.append(CGFloat(NumberFormatter().number(from: data[0])!))
                diastolicData.append(CGFloat(NumberFormatter().number(from: data[1])!))
                if data.count > 2 && data[2] != nil && data[2] != ""
                {
                heartrateData.append(CGFloat(NumberFormatter().number(from: data[2])!))
                }
                xLabelData.append(getDateForGraph(date: measurement.measurementDateTime!))
            }
            
            if systolicData.count > 0 {
                chartView.clearAll()
                chartView.x.labels.visible = true
                chartView.y.labels.visible = true
                chartView.x.labels.values = xLabelData
                chartView.addLine(systolicData)
                chartView.addLine(diastolicData)
                chartView.addLine(heartrateData)
            }
            
        }else if viewType == 2 {
            var pulseData : [CGFloat] = []
            var spo2Data : [CGFloat] = []
            
            for measurement in measurementDataArray {
                let data: [String] =  (measurement.value?.components(separatedBy: "/"))!
                pulseData.append(CGFloat(NumberFormatter().number(from: data[0])!))
                if data.count > 1 && data[1] != ""{
                    spo2Data.append(CGFloat(NumberFormatter().number(from: data[1])!))
                }
                xLabelData.append(getDateForGraph(date: measurement.measurementDateTime!))
            }
            
            if pulseData.count > 0 {
                chartView.clearAll()
                chartView.x.labels.visible = true
                chartView.y.labels.visible = true
                chartView.x.labels.values = xLabelData
                chartView.addLine(pulseData)
                chartView.addLine(spo2Data)
            }
            
        }else{
            var chartData : [CGFloat] = []
            for measurement in measurementDataArray {
                chartData.append(CGFloat((measurement.value as! NSString).doubleValue))
                xLabelData.append(getDateForGraph(date: measurement.measurementDateTime!))
            }
            if chartData.count > 0 {
                chartView.clearAll()
                chartView.x.labels.visible = true
                chartView.y.labels.visible = true
                chartView.x.labels.values = xLabelData
                chartView.addLine(chartData)
            }
        }
        
    }
    
     func bluetoothDevicedidcapturemeasurment(_ measurementType: String!, valuedict valuesdictionary: [AnyHashable : Any]) {
            var measurement: MeasurementData! = MeasurementData()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
    
            measurement.measurementName = "BloodPressure";
            request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
            request.returnsObjectsAsFaults = false
    
            measurement.isManual = false
    
            do {
                let result = try context.fetch(request)
                for data in result as! [NSManagedObject] {
                    measurement.measurementTypeId = data.value(forKey: "id") as? String
                    measurement.measurementUnit = data.value(forKey: "unit") as? String
                }
            }catch {
                print("Failed")
            }
            let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            request.returnsObjectsAsFaults = false
            do {
                let result = try context.fetch(patientDetailsRequest)
                for data in result as! [NSManagedObject] {
                    measurement.patientId = data.value(forKey: "id") as? String
                }
            } catch {
                print("Failed")
            }
            measurement.measurementDateTime = getCurrentDateTimeasString()
    
            let systolicVal = valuesdictionary["systolic"] as! Int
            let diaSystolicVal = valuesdictionary["diastolic"] as! Int
            let heartRateValue = valuesdictionary["pulse"] as! Int
            measurement.measurementValueType = "2/3"
            measurement.value =  String(systolicVal) + "/" + String(diaSystolicVal) + "/" + String(heartRateValue)
            let linkedId =  UUID().uuidString
            measurement.linkedId = linkedId

            CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
            NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
            }
            print(valuesdictionary)
    
        }
    
    func getCurrentDateTimeasString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        
        return str
    }
    
    func getDateascontinuesStrings(datefromfunction:String) -> (String)
    {
        let dateFormatter = DateFormatter()
        var date:Date? = nil
        dateFormatter.locale = Locale.current
        // save locale temporarily
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" //Z has been removed since if we download user data again we are not receiving SSSZ and app crashes
        date = dateFormatter.date(from:datefromfunction)
        /*
         Temporaray fix to handle both mobile and server values
         */
        if date == nil{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //date = dateFormatter.date(from: datefromfunction)
            date = dateFormatter.date(from:datefromfunction)

        }

        let dateFormatterWithDay = DateFormatter()
        dateFormatterWithDay.dateFormat = "EEEE MMM d, yyyy HH:mm"
        let dateString = dateFormatterWithDay.string(from: date!)
        return dateString
    }
    
    func getDate(getdate:String) -> String
    {
        let dateFormatter = DateFormatter()
        var date:Date? = nil
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        date = dateFormatter.date(from:getdate)
        
        let dateFormaterwithday = DateFormatter()
        dateFormatter.dateStyle = .medium
        if date != nil{
            let dateToReturn = dateFormatter.string(from: date!)
            return dateToReturn
        }
        else
        {
            return ""
        }
    }
    
    func getTime(getTime:String) -> String
    {
        let dateFormatter = DateFormatter()
        var date:Date? = nil
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        date = dateFormatter.date(from:getTime)
        
        let dateFormaterwithday = DateFormatter()
        dateFormatter.timeStyle = .short
        if date != nil{
        let TimeToReturn = dateFormatter.string(from: date!)
        return TimeToReturn
        }else{
            return ""
        }
    }
    

    open var colors: [UIColor] = [
        UIColor(red: 0.121569, green: 0.466667, blue: 0.705882, alpha: 1),
        UIColor(red: 1, green: 0.498039, blue: 0.054902, alpha: 1),
        UIColor(red: 0.172549, green: 0.627451, blue: 0.172549, alpha: 1),
        UIColor(red: 0.839216, green: 0.152941, blue: 0.156863, alpha: 1),
        UIColor(red: 0.580392, green: 0.403922, blue: 0.741176, alpha: 1),
        UIColor(red: 0.54902, green: 0.337255, blue: 0.294118, alpha: 1),
        UIColor(red: 0.890196, green: 0.466667, blue: 0.760784, alpha: 1),
        UIColor(red: 0.498039, green: 0.498039, blue: 0.498039, alpha: 1),
        UIColor(red: 0.737255, green: 0.741176, blue: 0.133333, alpha: 1),
        UIColor(red: 0.0901961, green: 0.745098, blue: 0.811765, alpha: 1)
    ]
    
    func getAttributedStringForChart(mainString: String, strigToBeAttributed:String, color:UIColor) -> NSMutableAttributedString {
        let range = (mainString as NSString).range(of: strigToBeAttributed)
        let attributedString = NSMutableAttributedString(string:mainString)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        return attributedString
    }

}
