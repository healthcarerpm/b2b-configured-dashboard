//
//  DashboardViewController.swift
//  OnboardingExample
//
//  Created by Isham on 20/03/2019.
//  Copyright © 2019 Anitaa. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import SWRevealViewController


class DashboardViewController : UIViewController, WeightScaleMeasurementCaptureProtocol,BluetoothReadingViewControllerDelegate,NoninInputViewControllerDelegate, UIPopoverPresentationControllerDelegate, LogoutModalDelegate {
    
    var viewModel: SessionModel!
    
    var titleView:TitleView?
    
    @IBOutlet weak var MessagebadgeButton: UIButton!
    
    //ui window
    var window: UIWindow?
    //swRevealContainer
    var swContainer : SWRevealViewController?
    
    
    @IBOutlet weak var sideMenuBar: UIBarButtonItem!
    @IBOutlet weak var blueMessageBtn: UIButton!
    @IBOutlet weak var collectionViewCards: UICollectionView!
    
    @IBOutlet weak var vitalsSubHeader: UILabel!
    @IBOutlet weak var vitalsMainHeader: UILabel!
    @IBOutlet weak var lineChart: LineChart!
    @IBOutlet weak var logoutBtn: UIButton!
//    @IBOutlet weak var vitalsLbl: UILabel!
//    @IBOutlet weak var sessionsUmmary: UILabel!
    @IBOutlet weak var labelNameTitle: UILabel!
    let coreDataUtils = CoreDataUtils()

    @IBOutlet weak var scheduledSessionsLabel: UILabel!
    @IBOutlet weak var sessionsHeader: UILabel!

    @IBOutlet weak var lastLoginLabel: UILabel!
      
    @IBOutlet weak var startMySession: UIButton!
    
    @IBOutlet weak var firstSubVitalLabel: UILabel!
    
    @IBOutlet weak var firstSubVitalRecLabel: UILabel!
    @IBOutlet weak var secondSubVitalLabel: UILabel!
    
    @IBOutlet weak var secSubVitalRecLabel: UILabel!
    @IBOutlet weak var thirdSubVitalLabel: UILabel!
    
    @IBOutlet weak var thirdSubVitalRecLabel: UILabel!
    @IBOutlet weak var fourthSubVitalLabel: UILabel!
    
    @IBOutlet weak var fourthSubVitalRecLabel: UILabel!
    var viewType = 0
    
    var repeatingTimerControl : Bool = true
    
    var tapGesture = UITapGestureRecognizer()

    let list = ["Blood pressure","Blood glucose","Pulse","Temperature","Weight"]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func Logout(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Logout", message: "Are you sure you wan to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Logout",
                                      style:.default,
                                      
                                      handler: {(alert: UIAlertAction!) in self.deleteAllData()
                                        self.repeatingTimerControl = false
                                        print("timer on/off: \(self.repeatingTimerControl)")
                                        UserDefaults.standard.set("no", forKey: "LoggedIn")
                                        
                                        UserDefaults.standard.synchronize()
                                        
                                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                        
                                        let loginVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                                        
                                        self.present(loginVC, animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "cancel",
                                      
                                      style:.cancel,
                                      
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func deleteAllData() {
          
          do {
              //invalidate the timer on logout
              MeasurementIdentifier.sharedInteractor.repeatingTimer.suspend()
              try self.coreDataUtils.deleteAllDatabase()
              
          } catch  {
              print("caught exception")
          }
          
      }
    
    func handleTouch() {
                let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Logout",
                                      style:.default,
                                      
                                      handler: {(alert: UIAlertAction!) in self.deleteAllData()
                                        self.repeatingTimerControl = false
                                        print("timer on/off: \(self.repeatingTimerControl)")
                                        UserDefaults.standard.set("no", forKey: "LoggedIn")
                                        
                                        UserDefaults.standard.synchronize()
                                        
                                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                        
                                        let loginVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                                        
                                        self.present(loginVC, animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "cancel",
                                      
                                      style:.cancel,
                                      
                                      handler: nil))

        
        self.present(alert, animated: true, completion: nil)
    }
    
    func moveToTC() {
        let tcview =  self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionsViewController") as! TermsConditionsViewController
        tcview.fromDashboard = true
        self.present(tcview, animated: true, completion: nil)
    }

    
    override func viewDidLayoutSubviews() {

    }
    
    @objc func UpdateBtnMessage() {

        //Add IF conditions according to what your button should do in different cases here.
     let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
      let nextViewController = storyBoard.instantiateViewController(withIdentifier: "messageDetail") as! ChatViewController
      self.navigationController?.show(nextViewController, sender: self)

    }
    
    
    override func viewDidLoad() {
        //UI
        
        blueMessageBtn.addTarget(self, action: #selector(UpdateBtnMessage), for: .touchUpInside)
        
        self.setUpUI()
        //Functions
        NetworkManager.sharedInstance.startObservingforNetworkReachability()
        
        NetworkManager().getpatientLastMessagesreadtime(userId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
            _ in
            
        })
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL dd"
        
        let patientDetails = coreDataUtils.getUserDetails();
        if(patientDetails.firstName != nil){
            //.uppercased() removed
            self.navigationController?.navigationBar.topItem?.title = ("Hello, " + patientDetails.firstName! + "!")
        }
        else if RPMUserSettings.sharedInstance.firstname != "" || RPMUserSettings.sharedInstance.lastname != ""{
            //.uppercased() removed
            self.navigationController?.navigationBar.topItem?.title = ("Hello, " + RPMUserSettings.sharedInstance.firstname + "!")
        }
        else{
            //.uppercased() removed
            self.navigationController?.navigationBar.topItem?.title = "Hello!"
        }
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        MeasurementIdentifier.sharedInteractor.repeatingTimer.eventHandler = {
            if self.repeatingTimerControl{
                
                print("Timer Fired: \(self.repeatingTimerControl)")
                //get patient sessions from server
                NetworkManager().getPatientSessions(patientId: patientDetails.patientId!,completion:{ result in
                    
                    self.loadTableViewModel()
                })
                
                NetworkManager().postAssessmentResponse(patientId: patientDetails.patientId!, completion: { _ in })
                if(false){   //I know this makes no sense, but it works. Go figure...
                    MeasurementIdentifier.sharedInteractor.repeatingTimer.suspend()
                }
                
                NetworkManager().getpatientLastMessagesreadtime(userId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
                    _ in
                    
                })
                
                NetworkManager().getpatientmessages(patientId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
                    _ in
                    let newmessages = MeasurementIdentifier.sharedInteractor.messages!.count - MeasurementIdentifier.sharedInteractor.initialmessagecounter
                    //                    self.scheduledSessionsLabel.text = String(MeasurementIdentifier.sharedInteractor.initialmessagecounter )
                    self.calculateActualNewMessages()
                    //    self.setTabBadge(itemCount:String(newmessages))
                    
                })
                
                
            }
        }
        
        MeasurementIdentifier.sharedInteractor.repeatingTimer.resume()
        
        NetworkManager().getPatientSessions(patientId: patientDetails.patientId!,completion:{ result in
            
            self.loadTableViewModel()
            
        })
        
        DispatchQueue.main.async {
            NetworkManager().postAssessmentResponse(patientId: patientDetails.patientId!, completion: { _ in })
            
        }
        
        
        NetworkManager().getpatientmessages(patientId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
            _ in
            
            let newmessages = MeasurementIdentifier.sharedInteractor.messages!.count - MeasurementIdentifier.sharedInteractor.initialmessagecounter
            
            self.scheduledSessionsLabel.text = String(MeasurementIdentifier.sharedInteractor.initialmessagecounter )
            if MeasurementIdentifier.sharedInteractor.messages!.count > 0{
                self.calculateActualNewMessages()
            }
            
        })
        
        
        if self.revealViewController() != nil {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.lineChartTapped(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        lineChart.addGestureRecognizer(tapGesture)
        lineChart.isUserInteractionEnabled = true
        self.presentInitialChart()
    }
    
    func calculateActualNewMessages()
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if MeasurementIdentifier.sharedInteractor.datetimelastRead != nil{
            let datelastread = formatter.date(from: MeasurementIdentifier.sharedInteractor.datetimelastRead!) as! Date
            
            print("Got date as \(datelastread)")
            MeasurementIdentifier.sharedInteractor.initialmessagecounter = 0
            for message in MeasurementIdentifier.sharedInteractor.messages!{
                let result = message.creationDate!
                let messageDate =  formatter.date(from:result)!
                if messageDate.compare(datelastread) == .orderedAscending{
                    print("Yedvat")
                }else if messageDate.compare(datelastread) == .orderedDescending{
                    print("Taleket")
                    MeasurementIdentifier.sharedInteractor.initialmessagecounter = MeasurementIdentifier.sharedInteractor.initialmessagecounter + 1
                }else {
                    print("Nothing else matters")
                }
            }
            self.setTabBadge(itemCount:String(MeasurementIdentifier.sharedInteractor.initialmessagecounter))
            
            self.scheduledSessionsLabel.text = String(MeasurementIdentifier.sharedInteractor.initialmessagecounter )
            
            self.blueMessageBtn.setTitle("You have \(MeasurementIdentifier.sharedInteractor.initialmessagecounter) unread messages. Click to read/send", for: .normal)
        }
    }
    
   
    @IBAction func navigateToMeasurementVC(_ sender: Any) {
        self.setViewType()
        if (viewType == 0){
            //BP
            let AndBPstoryboard = UIStoryboard(name: "BluetoothReading", bundle: nil)
            let nextVC = AndBPstoryboard.instantiateViewController(withIdentifier: "AnDBPStoryboard") as! BluetoothReadingViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
            
        else if (viewType == 2){
            //pulseOx
            let NoninStoryboard = UIStoryboard(name: "NoninPulseOxStoryboard" , bundle: nil)
            let nextVC = NoninStoryboard.instantiateViewController(withIdentifier: "NoninInputViewController") as! NoninInputViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
        else if(viewType == 4){
            //weight
            let AnDStoryboard = UIStoryboard(name: "andWeightScaleStoryboard" , bundle: nil)
            let nextVC = AnDStoryboard.instantiateViewController(withIdentifier: "A&DWeightScale") as! AnDWeightScaleViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
        else
        {
            self.showAlert(title: "Warning", message: "No device available")
        }
    }
    
    @IBAction func recordManuallyClicked(_ sender: UIButton) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "measureData") as! MeasureRecordsViewController
        nextViewController.viewType = self.viewType
        self.navigationController?.show(nextViewController, sender: self)
        
    }
        
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         //segue for the popover configuration window
        if segue.identifier == "popover" {
            
            if let controller = segue.destination as? LogoutModalViewController {
//                controller.popoverPresentationController!.delegate = self
                controller.preferredContentSize = CGSize(width: 320, height: 186)
            }
        }
        else if segue.identifier == "ConnectedCareSegue" {
                       let vc = segue.destination as! ConnectedCareViewController
                       vc.viewType = viewType
        }
    }
    
    var isPopoverPresented = false
    @objc func moreBarBtnAction(sender: UIBarButtonItem)
    {
        isPopoverPresented = !isPopoverPresented
        if !isPopoverPresented
        {
            if self.children.count > 0
            {
                let viewControllers:[UIViewController] = self.children
                for viewContoller in viewControllers{
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
            }
        }
        else{
            let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "LogoutModalViewController") as! LogoutModalViewController

            popoverContent.view.frame = CGRect(x: self.view.frame.midX, y: 100, width: UIScreen.main.bounds.width/2, height: 100)
            popoverContent.delegate = self
            self.addChild(popoverContent)
            self.view.addSubview(popoverContent.view)
            popoverContent.didMove(toParent: popoverContent)
        }
    }
    
    func addCategory() {

       
    }
    
    @objc func lineChartTapped(sender: UITapGestureRecognizer)
    {
        self.setViewType()
        self.performSegue(withIdentifier: "ConnectedCareSegue", sender: nil)
    }


    //MARK: - action
    @objc func hamburgerClicked(){
        //toggle frontVC on clicking hamburger menu
        revealViewController()?.revealToggle(animated: true)
    }
    
    
    func setUpUI()
    {

        self.startMySession.setTitle("No Session Scheduled", for: .disabled)
        self.startMySession.titleLabel?.adjustsFontForContentSizeCategory = true
        self.startMySession.titleLabel?.adjustsFontSizeToFitWidth = true
        self.startMySession.titleLabel?.lineBreakMode = .byWordWrapping
        self.startMySession.titleLabel?.numberOfLines = 1
        self.startMySession.titleLabel?.minimumScaleFactor = 0.2
        
        self.startMySession.layer.cornerRadius = 10
        self.startMySession.isEnabled = false
 //       self.startMySession.alpha = 0.25
        self.startMySession.clipsToBounds = true
        
        //setting only logo in navigation
        let imageView = UIImageView(frame: CGRect(x:UIScreen.main.bounds.size.width - 60, y:12, width:50, height:20))
        imageView.contentMode = .scaleAspectFit
        imageView.image = LogoResource.sharedInteractor.splashLogo
        navigationController?.navigationBar.addSubview(imageView)
        //option to set the logo that covers entire bar
        //navigationController?.navigationBar.setBackgroundImage(logo, for: .default)
        titleView = TitleView.instanceFromNib()
        navigationController?.navigationItem.titleView = titleView
        titleView!.title.text = "Dashboard"
        titleView!.layoutIfNeeded()
        

        

    }
    
    
    
    //Assessments from coredatautils are loaded into the current viewmodel
    func loadTableViewModel()
    {
        let coreDataUtils = CoreDataUtils()
        self.viewModel = SessionModel(assessments: coreDataUtils.getPendingAssessments())
        checkForSessionAvailability()
    }
    
    var sessionIndex = 0
    
    func checkForSessionAvailability(){
        var now = Date();
        let localDateformatter = DateFormatter()
        localDateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let utctolocalDatetimeString = UTCToLocalstring(date:localDateformatter.string(from: now) )
        if utctolocalDatetimeString.count != 0{
            now = localDateformatter.date(from: utctolocalDatetimeString)!
        }
        var nextSessionStartTime = Date.distantFuture;
        var nextSessionFlag = false;
        //n = today number of  PENDING sessions that are stored in the mobile(as of now)
        let n = self.viewModel.numberOfAssessments
        //to find out the number of PENDING sessions available today : not included in view Model as it is a nascent requirement
        var todaySessionCount = 0
        let date = Date() // current date or replace with a specific date
        let calendar = Calendar.current
        let dayEndTime = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: date)!
        if n > 0{
            for i in 0...n-1 {
                
                let localstarttime = UTCToLocalstring(date: localDateformatter.string(from: self.viewModel.startDateTime(at: i)))
                
                let localendtime = UTCToLocalstring(date: localDateformatter.string(from: self.viewModel.endDateTime(at: i)))
                
                let localstartdate = localDateformatter.date(from:localstarttime)!
                let localenddate = localDateformatter.date(from: localendtime)!
                
                if (localstartdate <= now && now <= localenddate  && ( self.viewModel.isSessionCompleted(at: i) == false && self.viewModel.isSessionSubmitted(at: i) == false) ){
                    todaySessionCount += 1
                }
                
            }
        }
        self.sessionsHeader.text = "You Have \(todaySessionCount) Sessions Due To Complete Today"
        //the looping below is a process to selection a session which has to be linked to Start session button
        if n > 0{
            for i in 0...n-1 {
                let localstarttime = UTCToLocalstring(date: localDateformatter.string(from: self.viewModel.startDateTime(at: i)))
                
                let localendtime = UTCToLocalstring(date: localDateformatter.string(from: self.viewModel.endDateTime(at: i)))
                
                let localstartdate = localDateformatter.date(from:localstarttime)!
                let localenddate = localDateformatter.date(from: localendtime)!

                if (localstartdate <= now && now <= localenddate  && ( self.viewModel.isSessionCompleted(at: i) == false && self.viewModel.isSessionSubmitted(at: i) == false) ){
                    self.startMySession.isEnabled = true
                    self.startMySession.setTitle("Click here to start your health session", for: .normal)
                    self.startMySession.alpha = 1
                    sessionIndex = i
                    startMySession.tag = sessionIndex
                    let headline = self.viewModel.text(at: i)
                    let assessmentId = self.viewModel.assessmentId(at: i)
                    let sessionId = self.viewModel.sessionId(at: i)
                    startMySession.accessibilityIdentifier = assessmentId
                    startMySession.accessibilityLabel = sessionId
                    startMySession.accessibilityHint = headline
                    //                    self.startMySession
                    break
                }else{
                    if(self.viewModel.startDateTime(at: i) > now && nextSessionStartTime > self.viewModel.startDateTime(at: i) &&  self.viewModel.isSessionCompleted(at: i) == false && self.viewModel.isSessionSubmitted(at: i) == false){
                        nextSessionStartTime = self.viewModel.startDateTime(at: i)
                        nextSessionFlag = true
                    }
                    self.startMySession.isEnabled = false
                self.startMySession.alpha = 0.50
                }
            }//end of for
            if(nextSessionFlag)
            {//set next session start time
                let dateTimeFormatter = DateFormatter()
                dateTimeFormatter.dateFormat="MMM-dd h:mm a"
                self.startMySession.setTitle("Next Health Session From:  \(dateTimeFormatter.string(from: nextSessionStartTime))", for: .disabled)
            }
        
        }//end of if
        else{
            //set empty session for button
            self.startMySession.isEnabled = false
            self.startMySession.alpha = 0.50
            self.startMySession.setTitle("No Session Scheduled", for: .disabled)
        }
        
    }
    
    @IBAction func onStartSessionClicked(sender: UIButton){
        let tag = sessionIndex
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "healthSessionViewController") as! HealthSessionViewController
        nextViewController.viewType = tag
        let coreDataUtils = CoreDataUtils()
        let nodes = coreDataUtils.getUntakenChildNodesOfAssessmentParent(assessmentId: self.viewModel.assessmentId(at: tag), sessionId:self.viewModel.sessionId(at: tag) , parentId: sender.accessibilityIdentifier ?? "")
        if(nodes.count > 0){
            //update  8-Oct-19
            //            self.repeatingTimerControl = false
            let finishedRootNodesCount = CoreDataUtils().getTakenChildNodesOfAssessmentParent(assessmentId:  sender.accessibilityIdentifier ?? "", sessionId:sender.accessibilityLabel ?? "", parentId: sender.accessibilityIdentifier ?? "").count
            
            nextViewController.viewModel = HealthSessionViewController.ViewModel(assessmentId: self.viewModel.assessmentId(at: tag),sessionId:self.viewModel.sessionId(at: tag), currentParentId: sender.accessibilityIdentifier ?? "", currentNode: nodes[0], questionName: sender.accessibilityHint!,completedQuestionNumber: finishedRootNodesCount, totalQuestions: (finishedRootNodesCount+nodes.count))
            self.navigationController?.show(nextViewController, sender: self)
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewwillappear dashboard")
              
        self.startMySession.setTitle("No Session Scheduled", for: .disabled)
        //        self.presentInitialChart()
        self.setMeasurementCount()
        self.setGraphValueForDashbord()
        //self.completedSessionsLabel.text = String(CoreDataUtils().getCompletedAssessmentCount())
        // self.missedSessionsLabel.text = String(CoreDataUtils().getMissedAssessmentsCount())
        self.loadTableViewModel()
        //        let lastSeen = UserDefaults.standard.bool(forKey: "seen")
        //        if lastSeen == true {
        self.scheduledSessionsLabel.text = "0"
        setTabBadge(itemCount:"0")
        //fix for the issue that the chart was not updated after each value update
        self.presentInitialChart()
        
        //        }
        //        else{
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 44.0/255.0, green: 171.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
//        self.navigationController?.navigationBar.topItem?.title = "CONNECTED CARE"
//        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont(name: "Montserrat", size: 22)!]
        if self.children.count > 0
        {
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
            }
        }
    }
    
    
    func presentInitialChart()
    {
        // simple line with custom x axis labels
        let xLabels: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
        
        lineChart.animation.enabled = true
        lineChart.area = true
        lineChart.x.labels.visible = false
        lineChart.x.grid.visible = false
        lineChart.y.grid.visible = false
        lineChart.x.labels.values = xLabels
        lineChart.y.labels.visible = false
        //        linechart.addLine(data)
        
        lineChart.x.axis.visible = false
        lineChart.y.axis.visible = false
        //        linechart.addLine(data2)
        
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        self.vitalsMainHeader.text = list[0]
        self.firstSubVitalLabel.text = "Blood Glucose"
        self.secondSubVitalLabel.text = "Temperature"
        self.thirdSubVitalLabel.text = "Weight"
        self.fourthSubVitalLabel.text = "Oxygen"
        let tableName = "BloodPressure"
        //                           if(self.list[indexPath.row] == "Blood pressure"){
        //                               tableName =
        //                           }else if (self.list[indexPath.row] == "Blood glucose"){
        //                               tableName = "BloodGlucose"
        //                           }else{
        //                               tableName = self.list[indexPath.row]
        //                           }
        let chartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: tableName)
        //clear line chart before displaying any data
        self.lineChart.clearAll()
        
        if(chartData != nil && chartData!.measurementArray.count > 0){
            self.vitalsSubHeader.text = String(chartData!.measurementArray.count) + " Records"
            //                               if(indexPath.row == 0 || indexPath.row == 2){
            lineChart.singleColoredLine = false
            lineChart.x.labels.values = Array(0...chartData!.measurementArray.count).map  { String($0) }
            lineChart.addLine(chartData!.measurementArray)
            lineChart.addLine(chartData!.secondaryMeasurementArray)
            //                               }else{
            //                                   cell.linechart.singleColoredLine = true
            //                                   cell.linechart.addLine(chartData!.measurementArray)
            //                               }
            //                               cell.linechart.isHidden = false
        }else{
            self.vitalsSubHeader.text = "0 Records"
            lineChart.isHidden = false
        }
        lineChart.setNeedsDisplay()
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.topItem?.title = "Dashboard"
//    }
    
    
    
    
    func setMeasurementCount(){
       
        let bloodGlucosechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "BloodGlucose")
        self.firstSubVitalRecLabel.text =   "(\(String(bloodGlucosechartData!.measurementArray.count)) Records)"
        let temperaturechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "Temperature")
        self.secSubVitalRecLabel.text =   "(\(String(temperaturechartData!.measurementArray.count)) Records)"
        let weightchartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "Weight")
        self.thirdSubVitalRecLabel.text =   "(\(String(weightchartData!.measurementArray.count)) Records)"
        let pulsechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "Pulse")
        self.fourthSubVitalRecLabel.text =   "(\(String(pulsechartData!.measurementArray.count)) Records)"
        
    }
    
    @IBAction func firstSubVitalClicked(_ sender: UIButton) {
        
        self.setMeasurement(measurementTitleLabel: self.firstSubVitalLabel, measurementRecordLabel: firstSubVitalRecLabel)

    }
    
    @IBAction func secondSubVitalClicked(_ sender: Any) {
        
        self.setMeasurement(measurementTitleLabel: self.secondSubVitalLabel, measurementRecordLabel: secSubVitalRecLabel)

    }
    
    
    @IBAction func thirdSubVitalClicked(_ sender: Any) {
        
        self.setMeasurement(measurementTitleLabel: self.thirdSubVitalLabel, measurementRecordLabel: thirdSubVitalRecLabel)

    }
    
    @IBAction func fourthSubVitalClicked(_ sender: Any) {
        
        self.setMeasurement(measurementTitleLabel: self.fourthSubVitalLabel, measurementRecordLabel: fourthSubVitalRecLabel)

    }
    
    
    //Set Measurement on Main Container View
    func setMeasurement(measurementTitleLabel: UILabel, measurementRecordLabel: UILabel){
        let titleLabelText = measurementTitleLabel.text
        let recordLabelText = measurementRecordLabel.text
        measurementTitleLabel.text = vitalsMainHeader.text
        measurementRecordLabel.text = vitalsSubHeader.text
        self.vitalsMainHeader.text = titleLabelText
        self.vitalsSubHeader.text = recordLabelText
        setViewType()
        self.setGraphValueForDashbord()
    }
    
    func setViewType(){
        
        switch vitalsMainHeader.text?.uppercased() {
        case "BLOOD PRESSURE" :
           viewType = 0
            break
        case "BLOOD GLUCOSE" :
            viewType = 1
            break
        case "OXYGEN" :
            viewType = 2
             break
        case "TEMPERATURE" :
           viewType = 3
            break
        case "WEIGHT" :
            viewType = 4
            break
        default :
            print("???")
            break
        }
        
    }
    func setGraphValueForDashbord(){
        
        var tableName = ""
        if(self.list[viewType] == "Blood pressure"){
            tableName = "BloodPressure"
        }else if (self.list[viewType] == "Blood glucose"){
            tableName = "BloodGlucose"
        }else{
            tableName = self.list[viewType]
        }
        let chartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: tableName)
        
        lineChart.clearAll()
        
        
        if(chartData != nil && chartData!.measurementArray.count > 0){
            vitalsSubHeader.text = "(\(String(chartData!.measurementArray.count)) Records)"
            if(viewType == 0 || viewType == 2){
              lineChart.singleColoredLine = false
                lineChart.x.labels.values = Array(0...chartData!.measurementArray.count).map  { String($0) }
                lineChart.addLine(chartData!.measurementArray)
                lineChart.addLine(chartData!.secondaryMeasurementArray)
            }else{
                lineChart.singleColoredLine = true
                lineChart.addLine(chartData!.measurementArray)
            }
            //lineChartVC.isHidden = false
        }else{
            vitalsSubHeader.text = "(0 Records)"
            //lineChartVC.isHidden = true
        }
       lineChart.setNeedsDisplay()
        
    }
    
 
    func setTimeOftheDay() -> String{
        
        let hour = Calendar.current.component(.hour, from: Date())
        var timeName: String
        
        switch hour {
        case 6..<12 : timeName = "Good Morning"
        case 12..<17 : timeName = "Good Afternoon"
        case 17..<22 : timeName = "Good Evening"
        default: timeName = "Good Night"
        }
        return timeName
    }
    
    @objc func onShowDetailsClicked(sender : UITapGestureRecognizer){
        let viewType = sender.view?.tag
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "detail") as! DetailsViewController
        nextViewController.viewType = viewType!
        self.navigationController?.show(nextViewController, sender: self)
    }
    
    @objc func onRecordManuallyClicked(sender: UIButton){
        let viewType = sender.tag
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "measureData") as! MeasureRecordsViewController
        nextViewController.viewType = viewType
        
        self.navigationController?.show(nextViewController, sender: self)
        
    }
    
    @objc func onRecordDeviceClicked(sender: UIButton){
        
        print("User click Approve button")
        let viewType = sender.tag
        print("ViewType \(viewType)")
        if (viewType == 0){
            //BP
            let AndBPstoryboard = UIStoryboard(name: "BluetoothReading", bundle: nil)
            let nextVC = AndBPstoryboard.instantiateViewController(withIdentifier: "AnDBPStoryboard") as! BluetoothReadingViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
            
        else if (viewType == 2){
            //pulseOx
            let NoninStoryboard = UIStoryboard(name: "NoninPulseOxStoryboard" , bundle: nil)
            let nextVC = NoninStoryboard.instantiateViewController(withIdentifier: "NoninInputViewController") as! NoninInputViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
        else if(viewType == 4){
            //weight
            let AnDStoryboard = UIStoryboard(name: "andWeightScaleStoryboard" , bundle: nil)
            let nextVC = AnDStoryboard.instantiateViewController(withIdentifier: "A&DWeightScale") as! AnDWeightScaleViewController
            self.navigationController?.show(nextVC, sender: self)
            nextVC.delegate = self
        }
        
    }
    
    @objc func onStartSurveyClicked(sender: UIButton){
        
    }
    
    @objc func onAllSessionsClicked(sender: UIButton){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "sessionListView") as! SessionListViewController
        self.navigationController?.show(nextViewController, sender: self)
        
    }
    
    func getDateForGraph(date: String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: date)!
        
        let graphDateFormatter = DateFormatter()
        graphDateFormatter.dateFormat = "MM-dd"
        return graphDateFormatter.string(from: date)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        collectionViewCards.collectionViewLayout.invalidateLayout()
//        collectionViewCards.reloadData()
        let patientDetails = coreDataUtils.getUserDetails();
        if(patientDetails.firstName != nil){
//            labelNameTitle.text = "Hello, " + patientDetails.firstName! + " " + patientDetails.lastName!
        }
        else if RPMUserSettings.sharedInstance.firstname != "" || RPMUserSettings.sharedInstance.lastname != ""{
//            labelNameTitle.text = "Hello, " + RPMUserSettings.sharedInstance.firstname + RPMUserSettings.sharedInstance.lastname
        }
        else{
//            labelNameTitle.text = "Hello, "
        }
        loadTableViewModel()
        let newmessages = MeasurementIdentifier.sharedInteractor.messages!.count - MeasurementIdentifier.sharedInteractor.initialmessagecounter
        self.scheduledSessionsLabel.text = String(MeasurementIdentifier.sharedInteractor.initialmessagecounter )
   //     self.setTabBadge(itemCount:String(newmessages))
        
    }
    
    ///device read delegates
    func weightScaleDidCaptureMeasurement(_ value:Double, dateCaptured date: Date) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Weight";
        request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeasString()
        
        let weight = value
        measurement.value =  String(weight)
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId
        
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
        }
        
    }
    
    func bluetoothDevicedidcapturemeasurment(_ measurementType: String!, valuedict valuesdictionary: [AnyHashable : Any]) {
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "BloodPressure";
        request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeasString()
        
        let systolicVal = valuesdictionary["systolic"] as! Int
        let diaSystolicVal = valuesdictionary["diastolic"] as! Int
        let heartRateValue = valuesdictionary["pulse"] as! Int
        measurement.measurementValueType = "2/3"
        measurement.value =  String(systolicVal) + "/" + String(diaSystolicVal) + "/" + String(heartRateValue)
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId
        
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
        }
        print(valuesdictionary)
        
    }
    
    func noninInputController(_ controller: NoninInputViewController!, didCompleteWithMeasurement measurements: [AnyHashable : Any]!) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Pulse";
        
        let table = "HeartRate"
        request.predicate = NSPredicate(format: "name = %@", table)// measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        
        measurement.measurementDateTime = getCurrentDateTimeasString()
        
        let pulse = measurements["pulse"] as! Int
        let spo2 = measurements["spo2"] as! Int
        measurement.value =  String(pulse) + "/" + String(spo2)
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId
        
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        
        measurement.value = String(pulse)
        
        NetworkManager().uploadDataPulse(measurementData: measurement ){ (response) -> () in
        }
        
        measurement.value = String(spo2)
        measurement.isManual = false
        measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.spo2ID
        
        NetworkManager().uploadDataSPO2(measurementData: measurement, completion: { (response) -> () in
        })
        
    }
    
    func showAlert (title: String, message: String){
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           self.present(alert, animated: true, completion: nil);
       }
    
    func getCurrentDateTimeasString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //        dateTimeFormatter.timeZone = TimeZone(identifier: "UTC")
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        return str
    }
    
    
    /**
     Update the isCompleted flag of each session(assessment).
     Calls getAssessments && getUntakenChildNodesOfAssessmentParent internally
     - Parameters:
     - patientId : Patient whose session is send
     - sessionId : The session being send
     - assessmentId: The assessment of the session being send.
     
     - Throws: .
     
     - Returns: none.
     */
    func markCompletedSessions2(){
        let coreDataUtils = CoreDataUtils()
        let assessments = coreDataUtils.getAssessments()
        
        for assessment in assessments{
            
            let nodes = coreDataUtils.getUntakenChildNodesOfAssessmentParent(assessmentId: assessment.assessmentId ?? "", sessionId: assessment.sessionId ?? "", parentId: assessment.assessmentId ?? "")
            
            if(nodes.count == 0){
                //session is completed
                coreDataUtils.markSessionAsComplete(sessionId: assessment.sessionId, assessmentId: assessment.assessmentId)
            }
        }
        
    }
    
    @IBAction func onStartSessionButtonClicked(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 1
    }
    
    func setLastLoginDate(date: String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: date)
        
        let graphDateFormatter = DateFormatter()
        graphDateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
        if date != nil{
            lastLoginLabel.text =  "Last action: " + graphDateFormatter.string(from: date!)
        }
    }
    
    func setTabBadge(itemCount:String) {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            
            tabItem.badgeValue = itemCount
        }
    }
    
//    func addBadge(itemvalue: String) {
//
//        let bagButton = BadgeButton()
//        bagButton.badgeTextColor = UIColor.white
//        bagButton.badgeBackgroundColor = UIColor.red
//        bagButton.frame = CGRect(x: self.blueMessageBtn.frame.minX + self.blueMessageBtn.frame.width, y: self.blueMessageBtn.frame.minY, width: 44, height: 44)
//        bagButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
//        bagButton.badge = itemvalue
//        self.blueMessageBtn.addSubview(bagButton)
//    }
    
}


class SessionModel {
    
    private var assessments: [Assessment]
    private lazy var dateFormatter: DateFormatter = {
        let fmtr = DateFormatter()
        fmtr.dateFormat = "EEEE, MMM d"
        return fmtr
    }()
    
    var numberOfAssessments: Int {
        return assessments.count
    }
    
    private func assessment(at index: Int) -> Assessment {
        return assessments[index]
    }
    
    func text(at index: Int) -> String {
        return assessment(at: index).name ?? ""
    }
    
    func sessionId(at index: Int) -> String {
        return assessment(at: index).sessionId
    }
    
    func assessmentId(at index: Int) -> String {
        return assessment(at: index).assessmentId
    }
    
    func isSessionCompleted(at index:Int) -> Bool{
        return assessment(at: index).isCompleted
    }
    
    func isSessionSubmitted(at index:Int) -> Bool{
        return assessment(at: index).isSubmitted
    }
    
    func date(at index: Int) -> String{
        guard let startDate = self.assessment(at: index).startTime else{
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MMM/yyyy"
        let result = dateFormatter.string(from: startDate)
        return result
    }
    
    
    func startTime(at index: Int) -> String{
        guard let startDate = self.assessment(at: index).startTime else{
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let result = dateFormatter.string(from: startDate)
        return result
        
        
    }
    
    func startDateTime(at index: Int) -> Date{
        guard let startDate = self.assessment(at: index).startTime else{
            return Date()
        }
        return startDate
    }
    
    func endTime(at index: Int) -> String{
        guard let endDate = self.assessment(at: index).endTime else{
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let result = dateFormatter.string(from: endDate)
        return result
    }
    
    func endDateTime(at index: Int) -> Date{
        guard let endDate = self.assessment(at: index).endTime else{
            return Date()
        }
        return endDate
    }
    
    //        func dueDateText(at index: Int) -> String {
    //            let date = node(at: index).dueDate
    //            return dateFormatter.string(from: date)
    //        }
    //
    //        func editViewModel(at index: Int) -> EditToDoItemViewController.ViewModel {
    //            let toDo = self.node(at: index)
    //            let editViewModel = EditToDoItemViewController.ViewModel(node: node)
    //            return editViewModel
    //        }
    //
    //        func addViewModel() -> EditToDoItemViewController.ViewModel {
    //            let node = Node()
    //            nodes.append(node)
    //            let addViewModel = EditToDoItemViewController.ViewModel(node: node)
    //            return addViewModel
    //        }
    //
    @objc private func removeNode(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let assessment = userInfo[Notification.Name.deleteNodeNotification] as? Assessment,
            let index = assessments.index(of: assessment) else {
                return
        }
        assessments.remove(at: index)
    }
    
    // MARK: Life Cycle
    init(assessments: [Assessment] = []) {
        self.assessments = assessments
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeNode(_:)), name: .deleteNodeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
